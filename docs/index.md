#Installation
TBD

#Development
To use a development/staging server you must first create a virtual machine
on which you will install all the containers and application. Just follow
these steps:
## 0. Create you access key
In the folder `deploy/keys` issue the following command:
```console
ssh-keygen -b 2048 -t rsa -f mysabor.key -q -N ""
```

This will create a public key (mysabor.key.pub) and a private key
(mysabor.key). You will need this files later.

## 1. Create a virtual machine
Create a virtual machine using Vagrant/VitualBox/vmWare and login as the root.
In the ~/.ssh/authorized_keys file paste the contents of mysabor.key.pub
Be sure that the permissions of the authorized_keys be 600 or it will not work.

## 2. Create an inventory file
The inventory file describes how to connect to your server. You can name your
file `sabor_inventory` and put in into the `deploy` folder. An example follows:
```
myserver ansible_ssh_host=10.10.10.88 ansible_ssh_user=root ansible_ssh_private_key_file=keys/mysabor.key
```
Be sure to enter the IP addres of your server as the `ansible_ssh_host` parameter.

## 3. Create the self-signed certificates for the virtual machine
Go to the `deploy/certs` create a self-signed certificate using the following
command:
```console
openssl req -new -nodes -x509 -subj "/C=HR/ST=Croatia/L=Zagreb/O=IT/OU=Web/CN=sabor2015.hr" -days 3650 -keyout mysabor2015.key -out mysabor2015.crt -extensions v3_ca
```
This will create the private key (mysabor2015.key) and a public certificate
(mysabor2015.crt), which you will need later.

## 4. Create server parameters
In the folder `deploy/host_vars` create a file named like your server (in the
examples it is `myserver`) containing specific configuration data for that
server:
```
#-------------------------------------------------------
# Server parameters
#-------------------------------------------------------
server_ssh_port: 22
server_network_interface: eth0
server_debug_toolbar: false
server_load_testing: false
server_password_secured: false


#-------------------------------------------------------
# PostgreSQL parameters
#-------------------------------------------------------
pg_opt_shared_buffers: 128MB
pg_opt_maintenance_work_mem: 256MB
pg_opt_effective_cache_size: 2GB

pg_sabor2015_user: sabor2015
pg_sabor2015_password: saborpassword
pg_sabor2015_dbname: sabor2015


#-------------------------------------------------------
# Application  parameters
#-------------------------------------------------------
app_number_web_backends: 2
app_number_celery_backends: 2


#-------------------------------------------------------
# nginx parameters
#-------------------------------------------------------
nginx_certificate_file: mysabor2015.crt
nginx_private_key_file: mysabor2015.key


#-------------------------------------------------------
# redis parameters
#-------------------------------------------------------
redis_memory_limit: 100MB


#-------------------------------------------------------
# Facebook parameters
#-------------------------------------------------------
facebook_app_id: 534897534895478
facebook_secret_key: sdofhwiofniwehfwe089fweufh
facebook_namespace: application-namespace
facebook_scope: user_birthday,user_friends,user_hometown,user_location
facebook_authorization_base_url: https://www.facebook.com/dialog/oauth
facebook_token_url: https://graph.facebook.com/oauth/access_token
facebook_redirect_uri: https://sabor2015.hr/oauth/fb

facebook_share_link: https://www.facebook.com/dialog/share
facebook_share_href: https://digitalocean.com
facebook_share_redirect_uri: https://sabor2015.hr/share/fb
```
Set the parameters to lower values if your virtual machine has less RAM than
1GB. Be sure to set real values for the `facebook_app_id`, 
`facebook_secret_key` and `facebook_namespace` from the application you have
configured on Facebook. The reddirect url-s must match the url of the website
you have entered on Facebook.

## 5. Init Ansible environment
To create the Ansible environment use the following command from `deploy`:
```console
./create-ansible-environment.sh
```
To enter the virtual environment use:
```console
source ansible_sandbox/bin/activate
```

## 6. Init the server
To init the server issue the following command after entering the virtual
environment in the `deploy` folder.
```console
ansible-playbook -i sabor_inventory setup_server.yml
```

## 7. Create the images for Docker
To create the images use the following command after entering the virtual
environment in the `deploy` folder:
```console
ansible-playbook -i sabor_invetory build_images.yml
```

## 8. Install the application
To install the application use the following command after entering the virtual
environment in the `deploy` folder:
```console
ansible-playbook -i sabor_invetory init_containers.yml
```

## 9. Change you host file
On the machine you run your browser change the `/etc/hosts` file to include
the following record:
```
10.10.10.88    sabor2015.hr
```
Be sure to set the IP correctly.

## 10. Profit
Open your browser to https://sabor2015.hr.
> As the certificate is self-signed, you browser may nag you a little.

## 11. Code changes
Once the application is running you can deploy source changes using the
following command after entering the virtual environment in the `deploy`
folder: 
```console
ansible-playbook -i sabor_invetory update_pyramid_app.yml
```

##Tunneling to the server
After installing the test server (10.10.10.88) you can map the services on it
to your local host to ease development with the following commands from the
`deploy` folder:
```console
ssh -f root@10.10.10.88 -L 5432:172.17.42.1:5432 -N -i keys/mysabor.key
ssh -f root@10.10.10.88 -L 6432:172.17.42.1:6432 -N -i keys/mysabor.key
ssh -f root@10.10.10.88 -L 5672:172.17.42.1:5672 -N -i keys/mysabor.key
ssh -f root@10.10.10.88 -L 6379:172.17.42.1:6379 -N -i keys/mysabor.key
```
> Provide a ssh key if you don't want to enter your password every time. 