#!/bin/sh

wget https://pypi.python.org/packages/source/v/virtualenv/virtualenv-13.1.2.tar.gz#md5=b989598f068d64b32dead530eb25589a
tar -xvf virtualenv-13.1.2.tar.gz
rm -rf ansible_sandbox
python virtualenv-13.1.2/virtualenv.py ansible_sandbox
ansible_sandbox/bin/pip install -r requirements.txt
rm -rf virtualenv-13.1.2
rm virtualenv-13.1.2.tar.gz
