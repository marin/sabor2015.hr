FROM debian:8.2
MAINTAINER marin@kset.org

RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys B97B0AFCAA1A47F044F244A07FCC7D46ACCC4CF8

RUN echo "deb http://apt.postgresql.org/pub/repos/apt/ jessie-pgdg main" > /etc/apt/sources.list.d/pgdg.list

RUN DEBIAN_FRONTEND=noninteractive apt-get update # 2015-10-09
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y postgresql-9.4=9.4.5-1.pgdg80+1
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y postgresql-client-9.4=9.4.5-1.pgdg80+1
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y postgresql-contrib-9.4=9.4.5-1.pgdg80+1

# delete unnecessary stuff
RUN DEBIAN_FRONTEND=noninteractive apt-get clean autoclean
RUN DEBIAN_FRONTEND=noninteractive apt-get autoremove -y

RUN update-rc.d -f postgresql remove

RUN rm -rf /var/log/postgresql/*
RUN rm -rf /var/lib/postgresql/*
RUN rm -rf /etc/postgresql/*
RUN rm -rf /var/run/postgresql/*

COPY fix_pg_permissions /usr/local/bin/fix_pg_permissions
RUN chmod 755 /usr/local/bin/fix_pg_permissions

RUN echo "Europe/Zagreb" > /etc/timezone && dpkg-reconfigure --frontend noninteractive tzdata

RUN echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
RUN echo "hr_HR.UTF-8 UTF-8" >> /etc/locale.gen
RUN locale-gen && dpkg-reconfigure --frontend noninteractive locales

RUN update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8

VOLUME ["/etc/postgresql", "/var/log/postgresql", "/var/lib/postgresql", "/var/run/postgresql", "/var/backup"]

CMD /usr/local/bin/fix_pg_permissions