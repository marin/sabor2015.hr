#!/bin/sh
set -e

if [ "$1" = 'pgbouncer' ]; then
	chown -R postgres /var/log/pgbouncer/
	chown -R postgres /var/run/pgbouncer/
	set -- gosu postgres /usr/sbin/pgbouncer /etc/pgbouncer/pgbouncer.ini
fi

exec "$@"

