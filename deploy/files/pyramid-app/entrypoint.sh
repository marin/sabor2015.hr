#!/bin/sh
set -e

if [ "$1" = 'supervisor' ]; then
    mkdir -p /var/log/supervisor/
    mkdir -p /var/run/supervisor/
    mkdir -p /var/log/sabor2015/
	set -- gosu root /app/env/current/bin/supervisord -c /app/supervisor.conf
fi

exec "$@"

