#!/bin/sh
set -e

if [ "$1" = 'nginx' ]; then
    mkdir -p /var/log/nginx/
    mkdir -p /var/run/nginx/
	chown -R nginx /var/log/nginx/
	chown -R nginx /var/run/nginx/
	chown -R nginx:nginx /etc/nginx/ssl/
	chmod 400 /etc/nginx/ssl/*
	set -- gosu root nginx
fi

exec "$@"

