import sabor2015
from sqlalchemy import engine_from_config
from sabor2015.lib.auth import SessionAuthenticationPolicy, FacebookUser
from sabor2015.lib.predicate import UserPresentPredicate
from sabor2015.lib.request import reference_token, session_user
from sabor2015.model import init_model
from pyramid.authorization import ACLAuthorizationPolicy
from pyramid.config import Configurator
from pyramid.security import Allow


class RootFactory(object):
    __acl__ = [(Allow, FacebookUser, 'vote')]

    def __init__(self, request):
        pass


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    engine = engine_from_config(settings, 'sqlalchemy.')
    init_model(engine)

    config = Configurator(settings=settings,
                          root_factory=RootFactory,
                          authentication_policy=SessionAuthenticationPolicy(),
                          authorization_policy=ACLAuthorizationPolicy())

    config.configure_celery(global_config['__file__'])
    config.add_renderer('.j2', 'pyramid_jinja2.renderer_factory')

    config.add_request_method(reference_token, reify=True)
    config.add_request_method(session_user, name='user', property=True)
    config.add_view_predicate('user_present', UserPresentPredicate)

    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('index:index', '/')
    config.add_route('info:mape', '/mape')
    config.add_route('info:faq', '/faq')
    config.add_route('info:pravila_privatnosti', '/pravila_privatnosti')
    config.add_route('info:uvjeti_koristenja', '/uvjeti_koristenja')
    config.add_route('oauth:fb', '/oauth/fb')
    config.add_route('oauth:logout', '/oauth/logout')
    config.add_route('share:fb', '/share/fb')
    config.add_route('api:friend_votes', '/api/friends/votes')
    config.add_route('api:global_votes', '/api/global/votes')
    config.add_route('api:region_votes', '/api/region/votes')
    config.add_route('api:user_info', '/api/me/info')
    config.add_route('api:election_regions', '/api/election/regions')
    config.add_route('api:election_regions_locations',
                     '/api/election/regions/locations')
    config.add_route('api:parties', '/api/parties')
    config.add_route('api:regions', '/api/regions')
    config.add_route('api:vote_in_time', '/api/votes/in_time')
    config.add_route('api:all', '/api/all')

    config.add_route('api:answer', '/api/answer')
    config.add_route('api:answer_extra', '/api/answer/extra')

    if 'sabor2015.load_testing' in settings and \
            settings['sabor2015.load_testing'] in ('on', 'yes', 'true',
                                                   'True'):
        config.add_view('sabor2015.views.load.new_user',
                        route_name='load_testing:new_user')
        config.add_route('load_testing:new_user', '/load/new_user')

    config.scan()
    return config.make_wsgi_app()
