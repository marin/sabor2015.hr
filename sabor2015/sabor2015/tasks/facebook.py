# -*- coding: utf-8 -*-
"""
Created on 2015-10-13

@author: marin
"""
from datetime import timedelta
from sabor2015.lib.redis_helper import JsonRedis
from sqlalchemy import engine_from_config, func, select
from celery import Task
from celery.signals import task_prerun
from facepy.exceptions import FacebookError
from facepy.graph_api import GraphAPI
import redis
from sabor2015.model import meta, init_model, User, Location, County
from pyramid_celery import celery_app as app
from celery.utils.log import get_task_logger
import requests
import json
import sabor2015.tasks.caching as redis_client

logger = get_task_logger(__name__)


@task_prerun.connect
def on_task_init(*args, **kwargs):
    if meta.engine:
        meta.engine.dispose()


class SaborTask(Task):
    abstract = True

    def __init__(self):
        if meta.engine is None:
            settings = app.conf['PYRAMID_REGISTRY'].settings
            engine = engine_from_config(settings, 'sqlalchemy.')
            init_model(engine)
        if redis_client.cashing is None:
            redis_client.cashing = JsonRedis.from_url(
                app.conf['PYRAMID_REGISTRY'].settings['redis.url']
            )
        if redis_client.locking is None:
            redis_client.locking = JsonRedis.from_url(
                app.conf['PYRAMID_REGISTRY'].settings['redis.locking.url']
            )

    def after_return(self, status, retval, task_id, args, kwargs, einfo):
        meta.Session.remove()

# @task_prerun.connect
# def on_task_init(*args, **kwargs):
#     if meta.engine:
#         meta.engine.dispose()


FACEBOOK_INFO_QUERY = "me?fields=id,name,age_range,gender," \
                      "hometown{location},location{location}," \
                      "birthday,friends.limit(100){id}"

GOOGLE_GEOLOCATION_API = URL = 'http://maps.googleapis.com/' \
                               'maps/api/geocode/json' \
                               '?latlng={},{}&sensor=false'


def process_location(data):
    if 'id' in data:
        location = Location.find_by_id(long(data['id']))
        if location is None:
            location = Location()
            location.id = long(data['id'])
        if 'location' in data:
            ldata=data['location']
            if 'city' in ldata:
                location.city = ldata['city']
            else:
                location.city = 'N/A'

            if 'country' in ldata:
                location.country = ldata['country']
            else:
                location.country = 'N/A'

            if 'latitude' in ldata:
                location.latitude = ldata['latitude']
            else:
                location.latitude = 0

            if 'longitude' in ldata:
                location.longitude = ldata['longitude']
            else:
                location.longitude = 0
        return location
    else:
        return None


def process_friends(user, friends):
    new_friends = []
    old_friends = user.friends or []
    while True:
        for record in friends['data']:
            new_friends.append(long(record['id']))
        if 'paging' in friends and 'next' in friends['paging']:
            req = requests.get(friends['paging']['next'])
            friends = json.loads(req.content)
        else:
            break

    new_friends.sort()
    user.friends = new_friends

    old_friends.sort()
    return new_friends != old_friends


@app.task(acks_late=True, base=SaborTask)
def rebuild_friendships(user_id):
    try:
        meta.Session.execute(func.rebuild_friendships(long(user_id))).scalar()
        meta.Session.commit()
    except Exception:
        logger.exception("Error rebuilding friendships for user %s", user_id)
        meta.Session.rollback()


@app.task(bind=True, acks_late=True, base=SaborTask)
def geolocate_location(self, location_id):
    try:
        #TODO: Add lock to location
        location = Location.find_by_id(location_id)
        if location is None:
            # this should not happen
            return
        url = GOOGLE_GEOLOCATION_API.format(location.latitude,
                                            location.longitude)
        r = requests.get(url)
        data = json.loads(r.content)
        location.geo_data = data

        for result in data['results']:
            if 'administrative_area_level_1' in result['types']:
                county = County.find_by_name(result['formatted_address'])
                if county is None:
                    county = County()
                    county.name = result['formatted_address']
                    meta.Session.add(county)
                location.county = county
                break

        for result in data['results']:
            if 'address_components' in result:
                for component in result['address_components']:
                    if 'postal_code' in component['types']:
                        location.zip = component['long_name']
                        break
            if location.zip is not None:
                break

        meta.Session.commit()

    except Exception as ex:
        logger.exception("Error fetching county for location %s", location_id)
        meta.Session.rollback()
        raise self.retry(countdown=2, exc=ex, max_retries=3)


@app.task(bind=True, acks_late=True, base=SaborTask)
def process_facebook_user(self, user_id, access_token):
    graph = GraphAPI(access_token)

    friend_list_changed = False
    try:
        r = graph.get(FACEBOOK_INFO_QUERY, retry=3)
        user = User.from_json(r)
        meta.Session.add(user)

        if 'location' in r:
            user.current_location = process_location(r['location'])

        if 'hometown' in r:
            user.hometown = process_location(r['hometown'])

        if 'friends' in r:
            friends = r['friends']
            friend_list_changed = process_friends(user, friends)

        meta.Session.commit()

        #issue backend tasks if needed
        if friend_list_changed:
                rebuild_friendships.delay(user_id)

        if user.current_location and user.current_location.geo_data is None:
            geolocate_location.delay(user.current_location.id)

        if user.hometown and user.hometown.geo_data is None:
            geolocate_location.delay(user.hometown.id)

    except Exception as ex:
        logger.exception("Error fetching data for user %s", user_id)
        meta.Session.rollback()
        raise self.retry(countdown=2, exc=ex, max_retries=3)


@app.task(acks_late=True, base=SaborTask)
def calculate_user_count():
    try:
        count = meta.Session.execute("SELECT count(*) FROM facebook_user;").scalar()
        redis_client.cashing.set('user_count', count)
    except Exception:
        logger.exception("Error counting users")

@app.task(acks_late=True, base=SaborTask)
def calculate_vote_count():
    try:
        count = meta.Session.execute("SELECT count(*) FROM vote;").scalar()
        redis_client.cashing.set('vote_count', count)
    except Exception:
        logger.exception("Error counting votes")


@app.task(acks_late=True, base=SaborTask)
def rematerialize_reach():
    try:
        meta.Session.execute("REFRESH MATERIALIZED VIEW CONCURRENTLY reach;")
        meta.Session.commit()
    except Exception:
        logger.exception("Error rematerializing reach.")
        meta.Session.rollback()

    try:
        results = meta.Session.execute("SELECT user_id, count, rank FROM reach;")
        for r in results:
            user_label = 'reach_{}'.format(r['user_id'])
            redis_client.cashing.set(user_label, r['count'] - 1)
            user_label = 'reach_rank_{}'.format(r['user_id'])
            redis_client.cashing.set(user_label, r['rank'])
    except Exception:
        logger.exception("Error fetching reach.")


@app.task(acks_late=True, base=SaborTask)
def rematerialize_total_reach():
    try:
        meta.Session.execute(
            "REFRESH MATERIALIZED VIEW CONCURRENTLY total_reach;")
        meta.Session.commit()
    except Exception:
        logger.exception("Error rematerializing total reach.")
        meta.Session.rollback()

    try:
        results = meta.Session.execute(
            "SELECT user_id, reach_count FROM total_reach;")
        for r in results:
            user_label = 'Treach_{}'.format(r['user_id'])
            redis_client.cashing.set(user_label, r['reach_count'] - 1)
    except Exception:
        logger.exception("Error retching total reach.")


@app.task(acks_late=True, base=SaborTask)
def rematerialize_friend_votes():
    try:
        meta.Session.execute(
            "REFRESH MATERIALIZED VIEW CONCURRENTLY friend_votes;")
        meta.Session.commit()
    except Exception:
        logger.exception("Error rematerializing friend votes.")
        meta.Session.rollback()

    try:
        results = meta.Session.execute(
            "SELECT user_id, aggregated_votes FROM friend_votes;")
        for r in results:
            _label = 'uf_votes_{}'.format(r['user_id'])
            redis_client.cashing.set(_label, r['aggregated_votes'])
    except Exception:
        logger.exception("Error fetching total reach.")


@app.task(acks_late=True, base=SaborTask)
def rematerialize_global_votes():
    try:
        meta.Session.execute(
            "REFRESH MATERIALIZED VIEW CONCURRENTLY global_votes;")
        meta.Session.commit()
    except Exception:
        logger.exception("Error rematerializing global votes.")
        meta.Session.rollback()

    try:
        results = meta.Session.execute(
            "SELECT id, aggregated_votes FROM global_votes;")
        for r in results:
            _label = 'global_votes_{}'.format(r['id'])
            redis_client.cashing.set(_label, r['aggregated_votes'])
    except Exception:
        logger.exception("Error fetching global votes.")


@app.task(acks_late=True, base=SaborTask)
def rematerialize_region_votes():
    try:
        meta.Session.execute(
            "REFRESH MATERIALIZED VIEW CONCURRENTLY region_votes;")
        meta.Session.commit()
    except Exception:
        logger.exception("Error rematerializing region votes.")
        meta.Session.rollback()

    try:
        results = meta.Session.execute(
            "SELECT region_id, aggregated_votes FROM region_votes;")
        for r in results:
            _label = 'region_votes_{}'.format(r['region_id'])
            redis_client.cashing.set(_label, r['aggregated_votes'])
    except Exception:
        logger.exception("Error fetching region votes.")


@app.task(acks_late=True, base=SaborTask)
def rematerialize_vote_in_time():
    try:
        meta.Session.execute(
            "REFRESH MATERIALIZED VIEW CONCURRENTLY vote_in_time;")
        meta.Session.commit()
    except Exception:
        logger.exception("Error rematerializing vote in time.")
        meta.Session.rollback()

    try:
        results = meta.Session.execute(
            "SELECT date_hour, votes FROM vote_in_time;")
        data = []
        for r in results:
            data.append({
                'time': r['date_hour'].strftime("%Y-%m-%d %H:00:00"),
                'votes': r['votes']
            })
        redis_client.cashing.set('vote_in_time', data)
    except Exception:
        logger.exception("Error fetching votes in time.")


@app.task(acks_late=True, base=SaborTask)
def rematerialize_user_rank():
    try:
        meta.Session.execute(
            "REFRESH MATERIALIZED VIEW CONCURRENTLY user_rank;")
        meta.Session.commit()
    except Exception:
        logger.exception("Error rematerializing user rank.")
        meta.Session.rollback()

    try:
        results = meta.Session.execute(
            "SELECT id, friend_count, rank FROM user_rank;")
        for r in results:
            label = "rank_{}".format(r['id'])
            data = {
                'friend_count': r['friend_count'],
                'rank': r['rank']
            }
            redis_client.cashing.set(label, data)
    except Exception:
        logger.exception("Error fetching rank data.")
