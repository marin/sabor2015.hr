# -*- coding: utf-8 -*-
"""
Created on 2015-11-01

@author: marin
"""
import random
import sys

from pyramid.httpexceptions import HTTPFound
from sabor2015.model import User, UserLogin
from sabor2015.model import meta


def new_user(request):
    user_id = random.randint(sys.maxint/2, sys.maxint)
    user = User.find_by_id(long(user_id))
    if user is None:
        user = User()
        user.id = user_id
        meta.Session.add(user)

    login = UserLogin.find_by_id(request.reference_token)
    if login is None:
        login = UserLogin()
        login.token_id = request.reference_token
        login.user = user
        meta.Session.add(login)
        meta.Session.commit()

    request.session['user_id'] = user_id
    request.session['user_name'] = 'Load Tester'

    return HTTPFound(request.route_path('index:index'))
