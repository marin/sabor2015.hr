from sabor2015.views import BaseView
from pyramid.security import effective_principals
from pyramid.view import view_config

class InfoView(BaseView):
    @view_config(route_name='info:mape', renderer='templates/mape.j2')
    def faq(self):
        return {
	}

    @view_config(route_name='info:faq', renderer='templates/faq.j2')
    def faq(self):
        return {
	}

    @view_config(route_name='info:pravila_privatnosti', renderer='templates/pravila_privatnosti.j2')
    def faq(self):
        return {
	}

    @view_config(route_name='info:uvjeti_koristenja', renderer='templates/uvjeti_koristenja.j2')
    def mape(self):
	return {
        }

