# -*- coding: utf-8 -*-
"""
Created on 2015-10-14

@author: marin
"""
import logging
from sabor2015.model import UserShare
from sabor2015.model import meta
from sabor2015.views import BaseView
from pyramid.view import view_config

logger = logging.getLogger(__name__)


class ShareView(BaseView):

    @view_config(route_name='share:fb', renderer='templates/share/fb.j2')
    def fb(self):

        error = None
        if 'error_code' in self.request.params:
            error = self.request.params['error_message']
        else:
            try:
                share = UserShare()
                share.token_id = self.request.reference_token
                share.share_type = 'fb'
                meta.Session.add(share)
                meta.Session.commit()
            except Exception:
                logger.exception("Error saving user FB share with token id %s",
                                 str(self.request.reference_token))
                meta.Session.rollback()

        return {
            'error': error
        }
