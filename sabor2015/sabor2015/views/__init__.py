# -*- coding: utf-8 -*-
"""
Created on 2015-10-12

@author: marin
"""


class BaseView(object):

    def __init__(self, context, request):
        self.context = context
        self.request = request
