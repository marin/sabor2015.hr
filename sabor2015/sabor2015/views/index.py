# -*- coding: utf-8 -*-
"""
Created on 2015-10-12

@author: marin
"""
from collections import namedtuple
import json
import uuid
import logging
from pyramid.httpexceptions import HTTPInternalServerError, HTTPServerError
from sabor2015.model import UserSession
from sabor2015.model import meta
from sabor2015.views import BaseView
from pyramid.view import view_config, notfound_view_config
from requests_oauthlib import OAuth2Session
from requests_oauthlib.compliance_fixes import facebook_compliance_fix

logger = logging.getLogger(__name__)

Referer = namedtuple("Referer", ['token_id', 'type'])


class IndexView(BaseView):

    def get_referer(self):
        if 'ref' in self.request.params:
            ref = self.request.params['ref']
            data = ref.split(',')
            if len(data) == 2:
                token_type = data[0]
                token_data = data[1]
                try:
                    token_id = uuid.UUID(token_data)
                    old_session = UserSession.find_by_id(token_id)
                    if old_session is not None:
                        if token_type in ['fb', 'tw']:
                            return Referer(token_id, token_type)
                except ValueError:
                    pass
        return None

    def get_authorization_url(self):
        settings = self.request.registry.settings
        facebook = OAuth2Session(
            settings['facebook.app_id'],
            redirect_uri=settings['facebook.redirect_uri'],
            scope=settings['facebook.scope'].split(','),
            state=self.request.session.new_csrf_token()
        )
        facebook = facebook_compliance_fix(facebook)
        authorization_url, state = facebook.authorization_url(
            settings['facebook.authorization_base_url'])

        return authorization_url

    @view_config(route_name='index:index',
                 renderer='templates/index/index.j2',
                 user_present=False)
    def index(self):
        authorization_url = self.get_authorization_url()

        user_session = UserSession.find_by_id(self.request.reference_token)
        if user_session is None:
            user_session = UserSession()
            user_session.token_id = self.request.reference_token
            referer_token = self.get_referer()
            if referer_token is not None:
                user_session.referer_token_id = referer_token.token_id
                user_session.referer_token_type = referer_token.type
            user_session.referer = self.request.referer
            meta.Session.add(user_session)
            meta.Session.commit()

        try:
            messages = json.loads(self.request.redis.get('messages') or "[]")
        except ValueError:
            messages = []

        return {
            'url': authorization_url,
            'user_count': self.request.redis.get('user_count') or 0,
            'vote_count': self.request.redis.get('vote_count') or 0,
            'messages': messages
        }

    @view_config(route_name='index:index',
                 renderer='templates/index/logged_in.j2',
                 user_present=True)
    def logged_in(self):
        user_id = self.request.user.id

        rank_data = self.request.redis.get("rank_{}".format(user_id))
        if rank_data is not None:
            rank_data = json.loads(rank_data)

        return {
            'user_count': self.request.redis.get('user_count') or 0,
            'vote_count': self.request.redis.get('vote_count') or 0,
            'rank_friends': rank_data['rank'] if rank_data is not None else 'N/A',
            'friend_count': rank_data['friend_count'] if rank_data is not None else 0,
            'reach': json.loads(self.request.redis.get(
                'reach_{}'.format(self.request.user.id)) or '0'),
            'reach_rank': json.loads(self.request.redis.get('reach_rank_{}'.format(user_id)) or '0'),

        }

    @notfound_view_config(renderer='templates/404.j2')
    def not_found(self):
        self.request.response.status = 404
        return {}

    @view_config(
        context=HTTPServerError,
        renderer='templates/500.j2')
    def internal_error(self):
        self.request.response.status = 500
        return {}

    # @view_config(
    #     context=Exception,
    #     renderer='templates/500.j2')
    # def other_errors(self):
    #     self.request.response.status = 500
    #     return {}