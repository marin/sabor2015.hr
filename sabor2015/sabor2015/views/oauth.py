# -*- coding: utf-8 -*-
"""
Created on 2015-10-12

@author: marin
"""
from sabor2015.model import User, UserLogin
from sabor2015.model import meta
from sabor2015.tasks.facebook import process_facebook_user
from sabor2015.views import BaseView
from pyramid.httpexceptions import HTTPFound
from pyramid.session import check_csrf_token
from pyramid.view import view_config
from requests_oauthlib import OAuth2Session
import json
from requests_oauthlib.compliance_fixes import facebook_compliance_fix


class OauthView(BaseView):

    @view_config(route_name='oauth:fb', check_csrf='state')
    def fb(self):
        #check_csrf_token(self.request, token='state')

        settings = self.request.registry.settings
        facebook = OAuth2Session(
            settings['facebook.app_id'],
            redirect_uri=settings['facebook.redirect_uri'],
            scope=settings['facebook.scope']
        )

        facebook = facebook_compliance_fix(facebook)

        #HACK: Without https requests fail
        import os
        os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'

        auth_token = facebook.fetch_token(
            settings['facebook.token_url'],
            client_secret=settings['facebook.secret_key'],
            authorization_response=self.request.url)

        r = facebook.get('https://graph.facebook.com/me?fields=id,name')
        data = json.loads(r.content)
        data['auth_token'] = auth_token  #TODO remove

        user = User.find_by_id(long(data['id']))
        if user is None:
            user = User()
            user.id = data['id']
            meta.Session.add(user)

        login = UserLogin.find_by_id(self.request.reference_token)
        if login is None:
            login = UserLogin()
            login.token_id = self.request.reference_token
            login.user = user
            meta.Session.add(login)
            meta.Session.commit()

        process_facebook_user.delay(data['id'], auth_token[u'access_token'])
        self.request.session['user'] = data  #TODO remove
        self.request.session['user_id'] = data['id']
        self.request.session['user_name'] = data['name']

        #return HTTPFound(location='https://localhost' + self.request.route_path('index:index'))
        return HTTPFound(self.request.route_path('index:index'))

    @view_config(route_name='oauth:logout')
    def logout(self):
        self.request.session.invalidate()
        return HTTPFound(self.request.route_path('index:index'))