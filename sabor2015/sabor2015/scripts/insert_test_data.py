# -*- coding: utf-8 -*-
"""
Created on 2015-10-21

@author: marin
"""
from collections import namedtuple
import datetime
from sqlalchemy import engine_from_config
import os
import sys
import random
from pyramid.paster import setup_logging, get_appsettings
from sabor2015 import init_model
from sabor2015.model import User, Location, Vote, \
    Region, Party, ElectionList, ElectionListRegions
from sabor2015.model import meta
from sabor2015.model.reference import ELECTION_LISTS, REGIONS, PARTIES

START_USER_ID = 1000000000000000L


def usage(argv):
    cmd = os.path.basename(argv[0])
    print('usage: %s <config_uri>\n'
          '(example: "%s development.ini")' % (cmd, cmd))
    sys.exit(1)

LocationTuple = namedtuple(
    'LocationTuple', ['id', 'city', 'zip', 'county',
                      'country', 'longitude', 'latitude'])

LOCATIONS_CSV = [
    '109492082410471;Karlovac;47000;6;Croatia;45.4833;15.55',
    '108139789207781;Vodnjan;52215;1;Croatia;44.9594;13.8517',
    '109920975697736;Belgrade;11000;;Serbia;44.8206;20.4622',
    '102167109824342;Rijeka;51000;3;Croatia;45.3431;14.4092',
    '116619061681465;Zagreb;10000;4;Croatia;45.8167;15.9833',
    '107795952581802;Osijek;31000;5;Croatia;45.5511;18.6939',
    '105594356142176;Krapina;49000;7;Croatia;46.1608;15.8789',
    '110988542258347;Sisak;44000;8;Croatia;45.4661;16.3783',
    '110838155611027;Varazdin;42000;9;Croatia;46.3044;16.3378',
    '108138402554052;Koprivnica;48000;10;Croatia;46.1628;16.8275',
    '106291972742515;Gospic;53000;11;Croatia;44.5461;15.3747',
    '103735029664971;Virovitica;33000;12;Croatia;45.8319;17.3839',
    '106287429409972;Pozega;34000;13;Croatia;45.3403;17.6853',
    '105581406142298;Slavonski Brod;35000;14;Croatia;45.1667;18.0167',
    '105634729469165;Zadar;23000;15;Croatia;44.1197;15.2422',
    '110591005627979;Sibenik;22000;16;Croatia;43.7272;15.9058',
    '109378492421228;Vukovar;32000;17;Croatia;45.3433;18.9997',
    '110647505623727;Split;21000;18;Croatia;43.5139;16.4558',
    '111891508829776;Dubrovnik;20000;19;Croatia;42.6506;18.0914',
    '114900901854652;Cakovec;40000;20;Croatia;46.3844;16.4339'
]
for l in LOCATIONS_CSV:
    loc = LocationTuple(*l.split(';'))
LOCATIONS = [LocationTuple(*l.split(';')) for l in LOCATIONS_CSV]


def create_locations():
    for l in LOCATIONS:
        location = Location.find_by_id(l.id)
        if location is None:
            location = Location()
            location.id = l.id
            location.city = l.city
            location.country = l.country
            location.latitude = l.latitude
            location.longitude = l.longitude
            meta.Session.add(location)

    meta.Session.commit()


def create_users(number_of_users):
    lists = meta.Session.query(ElectionList).all()
    for i in range(START_USER_ID, START_USER_ID + number_of_users):
        user = User.find_by_id(i)
        if user is None:
            user = User()
            user.id = i
            user.hometown_id = long(random.choice(LOCATIONS).id)
            user.current_location_id = long(random.choice(LOCATIONS).id)
            user.birthday = datetime.date(1920 + random.randint(0, 77), 1, 1)
            user.friends = list(
                set([START_USER_ID + random.randint(0, number_of_users)
                     for i in range(0, random.randint(1, 150))]))[0:-1]
            meta.Session.add(user)

        if user.vote is None:
            user.vote = Vote()
            user.vote.election_list = random.choice(lists)
            user.vote.region = random.choice(user.vote.election_list.regions).region
            user.vote.percentage = random.randint(0, 100)

        if i % 1000 == 999:
            meta.Session.commit()

    meta.Session.commit()


def create_regions():
    for r in REGIONS:
        region = Region.find_by_id(r['id'])
        if region is None:
            region = Region()
            region.id = r['id']
            region.name = r['label']
            meta.Session.add(region)

    meta.Session.commit()

def create_parties():
    for p in PARTIES:
        party = Party.find_by_id(p['party_id'])
        if party is None:
            party = Party()
            party.id = p['party_id']
            party.name = p['name']
            meta.Session.add(party)

    meta.Session.commit()


def create_election_lists():
    for l in ELECTION_LISTS:
        elist = ElectionList.find_by_id(l['list_id'])
        if elist is None:
            elist = ElectionList()
            elist.id = l['list_id']
            elist.name = l['short_name']
            elist.description = l['parties']
            for region_id in l['regions']:
                region = Region.find_by_id(region_id)

                if region is not None:
                    elregion = ElectionListRegions()
                    elregion.region = region
                    elist.regions.append(elregion)
            meta.Session.add(elist)

    meta.Session.commit()

def main(argv=sys.argv):
    if len(argv) < 3:
        usage(argv)
    config_uri = argv[1]
    setup_logging(config_uri)
    settings = get_appsettings(config_uri)
    engine = engine_from_config(settings, 'sqlalchemy.')
    init_model(engine)

    number_of_users = int(argv[2])
    random.seed(1)

    create_locations()
    create_regions()
    create_parties()
    create_election_lists()
    create_users(number_of_users)


if __name__ == '__main__':
    exit(main())