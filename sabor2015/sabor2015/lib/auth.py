# -*- coding: utf-8 -*-
"""
Created on 2015-10-14

@author: marin
"""
FacebookUser = u'facebook-user'


class SessionAuthenticationPolicy():

    def __init__(self, field_name='user_id'):
        self.field_name = field_name

    def unauthenticated_userid(self, request):
        if self.field_name in request.session:
            return request.session[self.field_name]
        else:
            return None

    def authenticated_userid(self, request):
        return self.unauthenticated_userid(request)

    def effective_principals(self, request):
        p = []
        if self.field_name in request.session:
            p.append(FacebookUser)
            p.append('o:{}'.format(request.session[self.field_name]))
        return p
