# -*- coding: utf-8 -*-
"""
Created on 2015-10-19

@author: marin
"""
from collections import namedtuple
import uuid


def reference_token(request):
    if 'reference_token' not in request.session:
        request.session['reference_token'] = uuid.uuid4()
    return request.session['reference_token']

ShortUser = namedtuple("ShortUser", ['id', 'name'])


def session_user(request):
    user_id = None
    if 'user_id' in request.session:
        user_id = request.session['user_id']

    user_name = None
    if 'user_name' in request.session:
        user_name = request.session['user_name']

    if user_id is None and user_name is None:
        return None
    else:
        return ShortUser(user_id, user_name)