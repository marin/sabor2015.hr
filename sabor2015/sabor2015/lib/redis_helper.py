# -*- coding: utf-8 -*-
"""
Created on 2015-10-28

@author: marin
"""
import json
from redis import StrictRedis


class JsonRedis(StrictRedis):
    def get(self, name):
        pickled_value = super(JsonRedis, self).get(name)
        if pickled_value is None:
            return None
        return json.loads(pickled_value)

    def set(self, name, value, ex=None, px=None, nx=False, xx=False):
        return super(JsonRedis, self).set(name, json.dumps(value), ex, px, nx, xx)