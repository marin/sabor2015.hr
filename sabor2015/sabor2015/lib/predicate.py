# -*- coding: utf-8 -*-
"""
Created on 2015-10-19

@author: marin
"""


class UserPresentPredicate(object):
    def __init__(self, val, config):
        self.present = bool(val)

    def text(self):
        return "user_present = %s" % self.present

    phash = text

    def __call__(self, context, request):
        if request.user is None and not self.present:
            return True
        elif request.user is not None and self.present:
            return True
        return False
