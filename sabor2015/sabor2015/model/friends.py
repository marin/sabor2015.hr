# -*- coding: utf-8 -*-
"""
Created on 2015-10-31

@author: marin
"""
from sabor2015.model import meta


def calculate_friend_votes(user_id):
    query = """
    SELECT array_to_json(array_agg(d.agg_vote)) AS aggregated_votes
    FROM (
        WITH friend_vote AS (
            SELECT v.election_list_id
            FROM vote v
            INNER JOIN friendship f ON f.to_id = v.user_id AND f.from_id = :user_id
        )
        SELECT array_to_json(ARRAY[el.id, count(fv.election_list_id)]) AS agg_vote
        FROM election_list el
        LEFT OUTER JOIN friend_vote fv ON el.id = fv.election_list_id
        GROUP BY el.id
    ) d
    """
    return meta.Session.execute(query, {'user_id': user_id}).first()[0]
