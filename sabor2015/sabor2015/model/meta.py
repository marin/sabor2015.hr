# -*- coding: utf-8 -*-
"""
Created on 2015-03-06

@author: Mladen Marinović
"""

# SQLAlchemy database engine. Updated by model.init_model()
engine = None

# SQLAlchemy session manager. Updated by model.init_model()
Session = None
