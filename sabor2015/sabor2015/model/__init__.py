# -*- coding: utf-8 -*-
"""
Created on 2015-10-15

@author: marin
"""
from datetime import datetime
from sqlalchemy.dialects.postgresql import UUID
import pytz
from sqlalchemy import event, Column, Table
from sqlalchemy.exc import InvalidRequestError
from sqlalchemy.ext.declarative import DeferredReflection, declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker, relationship, backref
import sys
from sabor2015.lib.exceptions import IzboriException
from sabor2015.model import meta
import re


class ReflectionBase(DeferredReflection):
    def __init__(self, **kwargs):
        for key in kwargs:
            # this is needed to check the attribute exists
            getattr(self, key)
            setattr(self, key, kwargs[key])

        super(ReflectionBase, self).__init__()

    @classmethod
    def all(cls):
        return meta.Session.query(cls).all()


Base = declarative_base(cls=ReflectionBase)


class County(Base):
    __tablename__ = 'county'

    @classmethod
    def find_by_name(cls, name):
        try:
            query = meta.Session.query(cls)
            result = query.filter(cls.name == name).first()
            return result
        except InvalidRequestError as ex:
            raise IzboriException(
                "Error retrieving county with name {}.".format(
                    name), ex), \
                None, sys.exc_info()[2]


class Location(Base):
    __tablename__ = 'location'

    county = relationship(
        County,
        primaryjoin="Location.county_id==County.id",
        lazy='joined'
    )

    @classmethod
    def find_by_id(cls, location_id):
        try:
            query = meta.Session.query(Location)
            result = query.filter(Location.id == location_id).first()
            return result
        except InvalidRequestError as ex:
            raise IzboriException(
                "Error retrieving location with id {}.".format(
                    location_id), ex), \
                None, sys.exc_info()[2]


class User(Base):
    __tablename__ = 'facebook_user'

    hometown = relationship(
        Location,
        primaryjoin="User.hometown_id==Location.id",
        lazy='joined'
    )

    current_location = relationship(
        Location,
        primaryjoin="User.current_location_id==Location.id",
        lazy='joined'
    )

    @classmethod
    def find_by_id(cls, user_id):
        try:
            query = meta.Session.query(User)
            result = query.filter(User.id == user_id).first()
            return result
        except InvalidRequestError as ex:
            raise IzboriException(
                "Error retrieving user with is {}.".format(
                    user_id), ex), \
                None, sys.exc_info()[2]

    @classmethod
    def from_json(cls, data):
        user = User.find_by_id(long(data['id']))
        if user is None:
            user = User()
            user.id = long(data['id'])

        if 'gender' in data and data['gender'] in ['male', 'female']:
            user.gender = 'm' if data['gender'] == 'male' else 'f'

        if 'birthday' in data:
            user.birthday_data = data['birthday']
            if user.birthday_data is not None:
                if len(user.birthday_data) == 4:  # only year
                    try:
                        birthday_year = datetime.strptime(
                            user.birthday_data, "%Y")
                        user.birthday = birthday_year
                    except ValueError:
                        pass
                else:  # try other
                    try:
                        birthday = datetime.strptime(
                            user.birthday_data, "%m/%d/%Y")
                        user.birthday = birthday
                    except ValueError:
                        pass

        if 'friends' in data:
            if 'summary' in data['friends']:
                if 'total_count' in data['friends']['summary']:
                    user.friend_count = data['friends']['summary']['total_count']

        return user


def update_created_modified_on_create_listener(mapper, connection, target):
    """
    Event listener that runs before a record is updated, and sets the
    create/modified field accordingly.
    """
    target.created_on = datetime.utcnow().replace(tzinfo=pytz.utc)
    target.modified_on = datetime.utcnow().replace(tzinfo=pytz.utc)

event.listen(User, 'before_insert', update_created_modified_on_create_listener)


def update_modified_on_update_listener(mapper, connection, target):
    """
    Event listener that runs before a record is updated, and sets the modified
    field accordingly.
    """
    # it's okay if this field doesn't exist - SQLAlchemy will silently
    # ignore it.
    target.modified_on = datetime.utcnow().replace(tzinfo=pytz.utc)


event.listen(User, 'before_update', update_modified_on_update_listener)


class UserSession(Base):
    __tablename__ = 'user_session'

    referer_session = relationship(
        'UserSession',
        primaryjoin="UserSession.referer_token_id==UserSession.token_id",
        lazy='select'
    )

    @classmethod
    def find_by_id(cls, token_id):
        try:
            query = meta.Session.query(cls)
            result = query.filter(cls.token_id == token_id).first()
            return result
        except InvalidRequestError as ex:
            raise IzboriException(
                "Error retrieving user session with token_id {}.".format(
                    token_id), ex), \
                None, sys.exc_info()[2]


class UserLogin(Base):
    __tablename__ = 'user_login'

    session = relationship(
        UserSession,
        primaryjoin="UserLogin.token_id==UserSession.token_id",
        lazy='select'
    )

    user = relationship(
        User,
        primaryjoin="UserLogin.user_id==User.id",
        foreign_keys='UserLogin.user_id',
        lazy='select'
    )

    @classmethod
    def find_by_id(cls, token_id):
        try:
            query = meta.Session.query(cls)
            result = query.filter(cls.token_id == token_id).first()
            return result
        except InvalidRequestError as ex:
            raise IzboriException(
                "Error retrieving user login with token_id {}.".format(
                    token_id), ex), \
                None, sys.exc_info()[2]



class UserShare(Base):
    __tablename__ = 'user_share'

    session = relationship(
        UserSession,
        primaryjoin="UserShare.token_id==UserSession.token_id",
        lazy='select'
    )


class Region(Base):
    __tablename__ = 'region'

    @classmethod
    def find_by_id(cls, region_id):
        try:
            query = meta.Session.query(cls)
            result = query.filter(cls.id == region_id).first()
            return result
        except InvalidRequestError as ex:
            raise IzboriException(
                "Error retrieving region with id {}.".format(
                    region_id), ex), \
                None, sys.exc_info()[2]


class Party(Base):
    __tablename__ = 'party'

    @classmethod
    def find_by_id(cls, party_id):
        try:
            query = meta.Session.query(cls)
            result = query.filter(cls.id == party_id).first()
            return result
        except InvalidRequestError as ex:
            raise IzboriException(
                "Error retrieving party with id {}.".format(
                    party_id), ex), \
                None, sys.exc_info()[2]


class ElectionListRegions(Base):
    __tablename__ = 'election_list_regions'

    region = relationship(
        Region,
        primaryjoin="ElectionListRegions.region_id==Region.id",
        lazy='select'
    )


class ElectionList(Base):
    __tablename__ = 'election_list'

    regions = relationship(
        ElectionListRegions,
        #secondary='ElectionListRegions',
        primaryjoin="ElectionList.id==ElectionListRegions.election_list_id",
        #secondaryjoin="ElectionListRegions.region_id==Region.id",
        lazy='select'
    )

    @classmethod
    def find_by_id(cls, list_id):
        try:
            query = meta.Session.query(cls)
            result = query.filter(cls.id == list_id).first()
            return result
        except InvalidRequestError as ex:
            raise IzboriException(
                "Error retrieving election list with id {}.".format(
                    list_id), ex), \
                None, sys.exc_info()[2]


class Vote(Base):
    __tablename__ = 'vote'

    user = relationship(
        User,
        primaryjoin="Vote.user_id==User.id",
        lazy='joined',
        backref=backref('vote', uselist=False)
    )

    election_list = relationship(
        ElectionList,
        primaryjoin="Vote.election_list_id==ElectionList.id",
        lazy='select'
    )

    region = relationship(
        Region,
        primaryjoin="Vote.region_id==Region.id",
        lazy='select'
    )


def vote_update_voted_on_update_listener(mapper, connection, target):
    """
    Event listener that runs before a record is updated, and sets the modified
    field accordingly.
    """
    # it's okay if this field doesn't exist - SQLAlchemy will silently
    # ignore it.
    target.voted_on = datetime.utcnow().replace(tzinfo=pytz.utc)


event.listen(Vote, 'before_update', vote_update_voted_on_update_listener)


@event.listens_for(Table, 'column_reflect')
def fix_user_session_token_id(inspector, table, column_info):
    """
    Fix UUID table reflection to return/accept UUID as object, and not string
    """
    if type(column_info.get('type')) == UUID:
        column_info.get('type').as_uuid = True


def init_model(engine):
    if meta.engine is not None:
        return

    meta.engine = engine
    meta.Session = scoped_session(sessionmaker(bind=engine,
                                               autocommit=False,
                                               autoflush=False))

    Base.prepare(engine)