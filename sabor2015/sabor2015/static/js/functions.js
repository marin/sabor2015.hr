
var sabor2015 = angular.module('sabor2015', []);

sabor2015.controller('sabor2015ctrl', ['$scope', '$http', function ($scope, $http) {

    $scope.user_info = {};

    $scope.setRegion = function(region) {
      $scope.user_info.vote_region = region;
      $scope.$apply();
    };

    $scope.setList = function(list) {
      $scope.user_info.vote_list = list;
      $scope.$apply();
    };

    $scope.setMeta = function(meta) {
      $scope.user_info.vote_meta = meta;
      $scope.$apply();
    };

    // Get voting statistics of friends
    $scope.getFriendsData = function(url) {
      $http({url: url, method: 'GET'})
        .success(function (data) {
          $scope.friends_data = data;
          $scope.error = ''; // clear the error messages

        })
        .error(function (data, status) {
          if (status === 404) {
            $scope.error = 'Database not available!';
          } else {
            $scope.error = 'Error: ' + status;
          }
        });
    };

    // Get global voting statistics
    $scope.getGlobalStatistics = function(url) {
      $http({url: url, method: 'GET'})
        .success(function (data) {
          $scope.global_statistics = data;
          $scope.error = ''; // clear the error messages

        })
        .error(function (data, status) {
          if (status === 404) {
            $scope.error = 'Database not available!';
          } else {
            $scope.error = 'Error: ' + status;
          }
        });
    };

    // Get region voting statistics
    $scope.getRegionStatistics = function(url) {
      $http({url: url, method: 'GET'})
        .success(function (data) {
          $scope.region_statistics = data;
          $scope.error = ''; // clear the error messages

        })
        .error(function (data, status) {
          if (status === 404) {
            $scope.error = 'Database not available!';
          } else {
            $scope.error = 'Error: ' + status;
          }
        });
    };

    // TODO: TOKEN AUTHENTIFICATION IN ANGULAR:
    //       https://auth0.com/blog/2014/01/07/angularjs-authentication-with-cookies-vs-token/
    //       https://docs.angularjs.org/api/ng/service/$http
    // TODO: TOKEN IS INJECTED BY SERVER INTO THE TEMPLATE, AND STORED IN document.cookie.

    // Send answers back to server
    $scope.sendAnswers = function(url,data) {
      $http({url: url, 
             data: data, 
             xsrfHeaderName: "X-CSRF-Token",
             xsrfCookieName: "csrf_token",
             method: 'POST'})
        .success(function (data, status, headers, config) {
          document.cookie = "csrf_token=" + data.token; // Update token

          // Because voting ended successfully we can retrieve all available statistics through individual api calls
          $('#results').show(1000);

          $scope.getFriendsData("api/friends/votes");
          $scope.getVotesInTime("api/votes/in_time");
          $scope.getGlobalStatistics("api/global/votes");
          $scope.getRegionStatistics("api/region/votes");
        })
        .error(function (data, status) {
          if (status === 404) {
            $scope.error = 'Database not available!';
          } else {
            $scope.error = 'Error: ' + status;
          }
        });
    };

    // Send answers to extra questions back to server
    $scope.sendAnswersExtra = function(url,data) {
      $http({url: url, 
             data: data, 
             xsrfHeaderName: "X-CSRF-Token",
             xsrfCookieName: "csrf_token",
             method: 'POST'})
        .success(function (data, status, headers, config) {
          document.cookie = "csrf_token=" + data.token; // Update token
        })
        .error(function (data, status) {
          if (status === 404) {
            $scope.error = 'Database not available!';
          } else {
            $scope.error = 'Error: ' + status;
          }
        });
    };

    // Data on user
    $scope.loadUserData = function(url) {
      $http({url: url, method: 'GET'})
        .success(function (data) {
          $scope.initial_user_info = data;
          $scope.error = ''; // clear the error messages

          // if (data.returning_user) { // Only what's important is whether the user voted or not!
          if (data.vote_list && (data.vote_meta || data.vote_meta==0 ) && data.vote_region) {
            $('#results').show(1000);
            $scope.getFriendsData("api/friends/votes");
            $scope.getVotesInTime("api/votes/in_time");

          // TODO: CHANGE THIS ENDPOINTS INTO CORRECT ONES!
          //       (format is the same as for friends so I'm using that)
          $scope.getGlobalStatistics("api/global/votes");
          $scope.getRegionStatistics("api/region/votes");
          }

        })
        .error(function (data, status) {
          if (status === 404) {
            $scope.error = 'Database not available!';
          } else {
            $scope.error = 'Error: ' + status;
          }
      });
    };

    // Load all data
    $scope.loadAllData = function(url) {
      $http({url: url, method: 'GET'})
        .success(function (data) {
          $scope.initial_user_info = data.user_info;
          $scope.parties = data.parties;
          $scope.election_regions = data.election_regions;
          $scope.regions = data.regions;

          $scope.error = ''; // clear the error messages

          // If user voted already then show him all the statistics
          if (data.user_info.vote_list && (data.user_info.vote_meta || data.user_info.vote_meta==0 ) && data.user_info.vote_region) {
            $('#results').show(1000);

            $scope.friends_data = data.friend_votes;
            $scope.votes_in_time = data.vote_in_time;
            $scope.global_statistics = data.global_votes;
            $scope.region_statistics = data.region_votes;

          }

        })
        .error(function (data, status) {
          if (status === 404) {
            $scope.error = 'Database not available!';
          } else {
            $scope.error = 'Error: ' + status;
          }
      });
    };

    // Data on political parties
    // $http({url: "../data/parties.json", method: 'GET'}) 
    $scope.loadPartiesData = function(url) {
      $http({url: url, method: 'GET'})
      .success(function (data) {
        $scope.parties = data;
        $scope.error = ''; // clear the error messages
      })
      .error(function (data, status) {
        if (status === 404) {
          $scope.error = 'Database not available!';
        } else {
          $scope.error = 'Error: ' + status;
        }
      });
    };

    // Data on election regions and lists in them
    // $http({url: "../data/election_regions.json", method: 'GET'}) 
    $scope.loadElectionRegionsData = function(url) {
      $http({url: url, method: 'GET'})
      .success(function (data) {
        $scope.election_regions = data;
        $scope.error = ''; // clear the error messages
      })
      .error(function (data, status) {
        if (status === 404) {
          $scope.error = 'Database not available!';
        } else {
          $scope.error = 'Error: ' + status;
        }
      });
    };

    // Data on election regions
    // $http({url: "../data/regions.json", method: 'GET'}) 
    $scope.loadRegionsData = function(url) {
      $http({url: url, method: 'GET'})
      .success(function (data) {
        $scope.regions = data;
        $scope.error = ''; // clear the error messages
      })
      .error(function (data, status) {
        if (status === 404) {
          $scope.error = 'Database not available!';
        } else {
          $scope.error = 'Error: ' + status;
        }
      });
    };

    // Data on votes in time
    $scope.getVotesInTime = function(url) {
      $http({url: url, method: 'GET'})
        .success(function (data) {
          $scope.votes_in_time = data;
          $scope.error = ''; // clear the error messages
        })
        .error(function (data, status) {
          if (status === 404) {
            $scope.error = 'Database not available!';
          } else {
            $scope.error = 'Error: ' + status;
          }
        });
    };

    // TODO: Total number of votes is injected into html through templating engine, not ajax.
    //       So this is for now best way to load it. Just make sure ".votes-all" element is always on the page.
    //       If it is not available then just make it random for now.
    // $scope.totalNumberOfVotes = Number($(".votes-all").text());

    // TODO: We are injecting user_count into global variable.
    //       But we don't want it to be zero or bad things will happen.
    //$scope.totalNumberOfVotes = vote_count || 76;
    $scope.totalNumberOfVotes = vote_count;

    // Data on election regions
    // $http({url: "../data/election_regions_locations.json", method: 'GET'}) 
    $scope.loadElectionRegionsLocations = function(url) {
      $http({url: url, method: 'GET'})
      .success(function (data) {
        $scope.election_regions_locations = data;
        $scope.error = ''; // clear the error messages
      })
      .error(function (data, status) {
        if (status === 404) {
          $scope.error = 'Database not available!';
        } else {
          $scope.error = 'Error: ' + status;
        }
      });
    };

}]);


// Whenever this element is preent on the page load user data in the global scope
sabor2015.directive('loadUserData', function ($parse) {
  return {
    restrict: 'E',
    replace: false,
    link: function (scope, element, attrs) {

      scope.loadUserData("api/me/info");

    }};

}); 

sabor2015.directive('cookiebar', function ($parse) {
  return {
    restrict: 'E',
    replace: false,
    link: function (scope, element, attrs) {

      $.cookieBar({
        message: 'Ova stranica koristi kolačiće kako bi Vam pružila bolje korisničko iskustvo. ',
        acceptText: 'Slažem se',
        domain: 'https://sabor2015.hr',
        referrer: 'https://sabor2015.hr',
        policyText: 'Pravila privatnosti',
        policyURL: 'https://sabor2015.hr/pravila_privatnosti'
      });

    }};

}); 

// Whenever this element is preent on the page load user data in the global scope
sabor2015.directive('loadAllData', function ($parse) {
  return {
    restrict: 'E',
    replace: false,
    link: function (scope, element, attrs) {

      scope.loadAllData("api/all");

    }};

}); 


// Generate short news articles whenever this element is on the page
sabor2015.directive('news', function ($parse) {
  return {
    restrict: 'E',
    replace: false,
    link: function (scope, element, attrs) {

      var news_title = attrs.title || 'title';
      var news_date = attrs.date || 'date';
      var news_text = attrs.text || 'text';

      $(element).append(
        $('<div>',{class:'row'}).append(
          $('<div>',{class:'col-md-12'})
            .append( '<hr>' )
            .append( $('<p>',{style:"font-weight:bold;"}).text(news_title)
               .append( $('<span>',{style:"font-weight:normal;"}).text(' ('+news_date+')') ) 
             )
            .append( $('<p>').text(news_text) )  
           )
      );

    }};

}); 


sabor2015.directive('regions', function ($parse) {
  return {
    restrict: 'E',
    replace: false,
    link: function (scope, element, attrs) {

      // size="2" is a workaround so that first option is not automatically selected
      // https://github.com/davidstutz/bootstrap-multiselect/issues/129
      $(element).html(
        '<select id="question-region" size="2"></select>'
      );

      // // All data is now loaded at the beginning of the page, so there is no need to load it here
      // scope.loadRegionsData(attrs.url);

      scope.$watch('regions', function (newData, oldData) {

        if (!newData) { return; }

        var regions = newData; // regions is also in controller scope

        regions.forEach( function(d) {
            $('#question-region').append(
              '<option value="' + d.id + '">' + d.label + '</option>'
              );
          });

        $("#question-region").multiselect({
          nonSelectedText: 'Odaberite izbornu jedinicu...',
          onChange: function(option, checked, select) {
            scope.setRegion($(option).val());
            }
        });

      });

      scope.$watchGroup(['regions','initial_user_info.vote_region'], function (newData, oldData) {

        if (!newData[0] || !newData[1]) { return; }
        $("#question-region").multiselect('select', newData[1]);

        // console.log('Inicijalna izborna jedinica je postavljena na ' + newData[1]);
      });

      scope.$watch('user_info.vote_region', function (newData, oldData) {

        if (!newData) { return; }

        // console.log('Korisnik je odabrao izbornu jedinicu ' + newData);
      });

    }};

}); 


sabor2015.directive('questionList', function ($parse) {
  return {
    restrict: 'E',
    replace: false,
    link: function (scope, element, attrs) {

      // size="2" is a workaround so that first option is not automatically selected
      // https://github.com/davidstutz/bootstrap-multiselect/issues/129
      $(element).html(
        '<select id="question-list" size="2"></select> ' + 
        '<div id="selected-list-label"></div>'
      );

      $("#question-list").multiselect({
        nonSelectedText: 'Prije izbora stranke odaberite izbornu jedinicu!',
        maxHeight: 200,
        onChange: function(option, checked, select) {
            scope.setList($(option).val());
          }
      });

      // Build dropdown menu for parties based on chosen election region
      var buildMenuLists = function (vote_region) {
          $('#question-list').empty();
          var parties_in_region = scope.election_regions.filter(function(d){return _.includes(d.regions,Number(vote_region));});
          parties_in_region.forEach( function(d) {
              $('#question-list').append(
                '<option value="' + d.list_id + '">' + d.short_name + '</option>'
                );
            });
          $("#question-list").multiselect('setOptions',{
            nonSelectedText: 'Odaberite stranku iz izborne jedinice ' + vote_region + '...'
          });
          $('#question-list').multiselect('rebuild');
      }

      var updateSelectedListLabel = function(vote_list) {
          $('#selected-list-label').empty();
          $('#selected-list-label').append('Odabrali ste sljedeću listu stranaka:')
          var temp_parties = _.pluck(_.where(scope.election_regions,{'list_id':Number(vote_list)}), 'parties');
          var parties = temp_parties[0].split(',').map(function(d){return d.trim();});

          parties.forEach( function(d) {
              $('#selected-list-label').append(
                '<p style="margin-bottom:0px;">' + d + '</p>'
                );
            });
      }

      // // All data is now loaded at the beginning of the page, so there is no need to load it here
      // scope.loadElectionRegionsData(attrs.url);

      scope.$watchGroup(['election_regions','initial_user_info.vote_region','initial_user_info.vote_list'], function (newData, oldData) {

        if ( !newData[0] || !newData[1] || !newData[2]) { return; }
        
        buildMenuLists(newData[1]); // new
        updateSelectedListLabel(newData[2]);
        
        $("#question-list").multiselect('select', newData[2]);

        // console.log('Inicijalna izborna jedinica za koju je korisnik glasao je ' + newData[1] + ' pa smo izgradili izbornik lista. Korisnik je glasao za stranku ' + newData[2]);

      });

      scope.$watch('user_info.vote_region', function (newData, oldData) {

        if ( !newData ) { return; }
        buildMenuLists(newData);

        // We have to reset the vote for list to some default value because that list maybe doesn't exist in the new region.
        // For some reason calling scope.setList() doesn't work:-/
        scope.user_info.vote_list = "-1"; // This works.

        $('#selected-list-label').empty();
        // console.log('Ups, netko je postavio novu izbornu jedinicu (' + newData + '), moramo resetirati liste!');

      });

      scope.$watch('user_info.vote_list', function (newData, oldData) {

        if ( !newData ) { return; }
        if (newData==-1) { return; } // This means that vote got reset after choosing new region, do nothing.
        updateSelectedListLabel(newData);
        // console.log('Korisnik je odabrao listu ' + newData + ' iz ' + scope.user_info.vote_region + ' izborne jedinice.');

      });

    }};

}); 


sabor2015.directive('questionMeta', function ($parse) {
  return {
    restrict: 'E',
    replace: false,
    link: function (scope, element, attrs) {

      $(element).html(
        '<div class="slider-row">' +
        '<input id="question-meta" type="text" data-slider-min="0"' + 
        ' data-slider-max="100" data-slider-step="1" data-slider-value="0" />' +
        '</div>' +
        '<p id="meta-label"></p>'
        );

      $("#question-meta").slider({
        ticks: [0, 25, 50, 75, 100],
        ticks_labels: ['0%', '25%', '50%', '75%', '100%'],
        ticks_snap_bounds: 1
      }
      );

      $("#question-meta").on('slideStop', function(e) {
        scope.setMeta(e.value);
      });

      $("#question-meta").on('change', function(e) {
        $('#meta-label').text('Odabrali ste ' + e.value.newValue + '%.');
      });
      

     scope.$watch('initial_user_info.vote_meta', function (newData, oldData) {

        if (!newData) { return; }

        $("#question-meta").slider('setValue',newData);
        $('#meta-label').text('Odabrali ste ' + newData + '%.');

        // console.log('Inicijalna vrijednost meta pitanja je ' + newData);
      });

    }};

}); 


sabor2015.directive('buttonVote', function ($parse) {
  return {
    restrict: 'E',
    replace: false,
    link: function (scope, element, attrs) {

      $(element).html(
        '<div style="height:5px;clear:both;"></div>' +
        '<p id="current-vote-label"></p>' +
        '<div style="height:5px;clear:both;"></div>' +
        '<button id="button-vote" class="btn btn-lg btn-default">Glasaj</button>'
      );

      $("#button-vote").click(function (e) {

        // User has to change at least one question (from his previous session) in order to submit a new vote
        // And the vote for list must not be -1 because this means that he changed a region in the meantime

        // TODO: UNDER ONE SESSION, USER CAN SUBMIT AS MANY ANSWERS AS HE WANTS, EVEN IF THEY ARE ALL THE SAME! 
        // TODO: IF USER CHANGES HIS VOTE AND THEN RETURNS THE OLD VALUES BEFORE SUBMITTING, HE WILL BE ABLE TO SUBMIT ALTHOUGH ANSWERS ARE THE SAME!

        if ( (scope.user_info.vote_region || scope.initial_user_info.vote_region ) && // region should be defined
             (scope.user_info.vote_list || scope.initial_user_info.vote_list ) && scope.user_info.vote_list!=-1 && // list should be defined and not reset (-1)
             (scope.user_info.vote_meta || scope.user_info.vote_meta==0 || scope.initial_user_info.vote_meta || scope.initial_user_info.vote_meta==0 ) && // meta should be defined, but it could also be 0
             (scope.user_info.vote_region || scope.user_info.vote_list || scope.user_info.vote_meta || scope.user_info.vote_meta==0 ) // at least one new information, otherwise no use in voting
             ) {

          // All values which are not set by the user in this session should be taken from initialization
          var region = scope.user_info.vote_region ? scope.user_info.vote_region : scope.initial_user_info.vote_region;
          var list = scope.user_info.vote_list ? scope.user_info.vote_list : scope.initial_user_info.vote_list;
          var meta = scope.user_info.vote_meta || scope.user_info.vote_meta==0 ? scope.user_info.vote_meta : scope.initial_user_info.vote_meta;

          var list_name = _.pluck(_.where(scope.election_regions,{'list_id':Number(list)}), 'short_name');
          $('#current-vote-label').text('Glasali ste za izbornu listu ' + list_name + ' (' + region + ' izborna jedinica) i predviđate da će dobiti ' + meta + '% glasova na izborima.');
        
          user_vote({'list': Number(list), 
                     'meta': Number(meta),
                     'region': Number(region)
                    });
        }
        else {
          $('#current-vote-label').html('<span style="color:red">Molim vas odgovorite na sva tri pitanja!</span>');
        }

      });

      var user_vote = function (d) {

        var list_name = _.pluck(_.where(scope.election_regions,{'list_id':Number(d.list)}), 'parties');
        // console.log('Glasali ste za stranku ' + list_name + ' (' + d.region + ' izborna jedinica) i predviđate da će dobiti ' + d.meta + ' glasova na izborima.');

        // TODO: This is supposed to be injected into main login template.
        d.user_id = user_id;

        // Send answers back to server
        scope.sendAnswers("api/answer",d);
        // scope.sendAnswers(d); // If url is defined in controlles (maybe a better solution?)

      }

    }};

}); 

sabor2015.directive('results', function ($parse) {
  return {
    restrict: 'E',
    replace: false,
    link: function (scope, element, attrs) {

     scope.$watch('friends_data', function (newData, oldData) {

        if (!newData) { return; }
        var friends_data = newData;

        // $(element).show(1000); // We show the results in loadUserData() after successfull retrieval of user votes

        // If friends_data is empty the result will be 0 because of the initial value of 0 in reduce
        // $('.votes-friends').text(
        //   friends_data.map(function(d){return d.votes})
        //               .reduce(function(prev,curr){return curr + prev;},0)
        // );
        $('.votes-friends').text(friends_data.votes_count);

        // If you have no friends we have to change the text
        if (Number($('.votes-friends').text())==0) {
          $('#friend-count-label').text('Nijedan od vaših prijatelja još nije glasao na ovoj aplikaciji:-(');
        }
        
        // If no users came through your share we have to change the text
        if (Number($('.shares-friends').text())==0) {
          $('#reach-count-label').text('Nijedan od korisnika nije došao preko vašeg sharea na ovu aplikaciju:-(');
        }


        if ($('.rank-friends').text()=='N/A.') {
          $('#friend-rank-label').empty();
        }
        
        if ($('.rank-shares').text()=='N/A.') {
          $('#reach-rank-label').empty();
        }

        
      });

      scope.$watch('region_statistics', function (newData, oldData) {

        if (!newData) { return; }
        var region_statistics = newData;

        $('.votes-region').text(region_statistics.votes_count);
        
      });

    }};

}); 


sabor2015.directive('questionExtra', function ($parse) {
  return {
    restrict: 'E',
    replace: false,
    link: function (scope, element, attrs) {

      $(element).html(
          '<select id="question-parties" multiple="multiple"></select>' +
          '<div style="height:5px;clear:both;"></div>' +
          '<p id="vote-parties-label"></p>' +
          '<div style="height:5px;clear:both;"></div>' +
          '<button id="button-vote-parties" class="btn btn-lg btn-default">Glasaj</button>'
        );

      $("#button-vote-parties").click(function (e) {

          // We can fetch selected parties in onChange callback of multiselect, but it's better to do it directly here.
          var selected_parties = [];
          $('#question-parties option:selected').each(function(i,d){
              selected_parties.push($(this).val()); // If we want text then use $(this).text()
          });

          if (selected_parties.length!=0) {
            var selected_parties_names = selected_parties.map(function(d){return _.pluck(_.where(scope.parties,{'party_id':Number(d)}),'name')[0];});
            $('#vote-parties-label').text('Sljedeće stranke simpatizirate: ');
            selected_parties_names.forEach(function(d) {
               $('#vote-parties-label').append('<p style="margin-bottom:0px;">' + d + '</p>');
            });

            user_vote_extra({'parties': selected_parties.map(function(d){return Number(d);})});
          }
          else {
            $('#vote-parties-label').html('<span style="color:red">Molim vas odaberite barem jednu stranku koju simpatizirate!</span>');
          }

      });

     var user_vote_extra = function (d) {

        // console.log('Simpatizirate sljedeće stranke: ' + d.parties );

        // TODO: This is supposed to be injected into main login template.
        d.user_id = user_id;

        // Send answers to extar question back to the server
        scope.sendAnswersExtra("api/answer/extra",d);

      }

      // // All data is now loaded at the beginning of the page, so there is no need to load it here
      // scope.loadPartiesData(attrs.url);

      scope.$watch('parties', function (newData, oldData) {

        if (!newData) { return; }
        
        newData.forEach( function(d) {
            $('#question-parties').append(
              '<option value="' + d.party_id + '">' + d.name + '</option>'
              );
          });

        $("#question-parties").multiselect({
          maxHeight: 200,
          enableCaseInsensitiveFiltering: true,
          buttonText: function(options, select) {
            if (options.length === 0) {
                return 'Odaberite stranke koje simpatizirate...';
            }
            else 
            if (options.length===1) {
                return  '1 stranka odabrana';
            }
            else if (options.length===2 || options.length===3) {
                return  options.length + ' stranke odabrane';
            }
            else {
                return options.length + ' stranki odabranih';
            }
          }
        });

      });

      // For returning users, after login we update the state of all questions based on what we retrieved from database.
      scope.$watchGroup(['parties','initial_user_info.vote_parties'], function (newData, oldData) {

        if (!newData[0] || !newData[1]) { return; }
        $('#question-parties').multiselect('select', newData[1]);
        
        // console.log('Inicijalne stranke koje se simpatiziraju su ' + newData[1]);
      });


    }};

}); 


sabor2015.directive('votesInTime', function ($parse) {
  return {
    restrict: 'E',
    replace: false,
    link: function (scope, element, attrs) {

     scope.$watch('votes_in_time', function (newData, oldData) {

        if (!newData) { return; }

        var data = newData;

        data.forEach(function(d) {
          d.time = d3.time.format("%Y-%m-%d %H:%M:%S").parse(d.time);
        });

        data.sort(function(a,b){return a.time-b.time;});

        var margin = {top: 20, right: 20, bottom: 50, left: 50},
            width = 600 - margin.left - margin.right,
            height = 200 - margin.top - margin.bottom;

        var x = d3.time.scale().range([0, width]);
        var y = d3.scale.linear().range([height, 0]);

        var xAxis = d3.svg.axis()
            .scale(x)
            .ticks(d3.time.day)
            .tickFormat(d3.time.format("%e.%m."))
            .orient("bottom");

        var yAxis = d3.svg.axis()
            .scale(y)
            .tickFormat(d3.format("d"))
            .orient("left");

        var line = d3.svg.line()
            .x(function(d) { return x(d.time); })
            .y(function(d) { return y(d.votes); });

        $(element).html(
          '<div id="votes-in-time"></div>'
        );

        $("#votes-in-time").empty();

        var svg = d3.select("#votes-in-time").append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
          .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        x.domain(d3.extent(data, function(d) { return d.time; }));
        y.domain(d3.extent(data, function(d) { return d.votes; }));

        svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis)
          .append("text")
            .attr("x", width/2) // .attr("dy", ".71em")
            .attr("dy", "3em")
            .style("text-anchor", "middle")
            .text("Vrijeme");

        svg.append("g")
            .attr("class", "y axis")
            .call(yAxis)
          .append("text")
            .attr("transform", "rotate(-90)") //.attr("y", 10)
            .attr("x", height/2)
            .attr("dx", "-6.5em")
            .attr("dy", "-2.5em")
            .style("text-anchor", "end")
            .text("Ukupni broj glasova");

        svg.append("path")
            .datum(data)
            .attr("class", "line")
            .attr("d", line);

      });

    }};

}); 


sabor2015.directive('progressChart', function ($parse) {
  return {
    restrict: 'E',
    replace: false,
    link: function (scope, element, attrs) {

      scope.$watch('totalNumberOfVotes', function (newData, oldData) {

        if (!newData) { return; }
        var totalNumberOfVotes = newData;
        
        $(element).empty();

        var width = 300,
          height = 300,
          radius = Math.min(width, height) / 2;

        var total_votes = 10000;

        var color_rim = d3.rgb(44,123,182); // bluish
        var color_center = d3.rgb(215,25,28); // redish
        var color_empty = d3.rgb(200, 200, 200);

        var svg = d3.select(element[0]).append("svg")
          .attr("width", width)
          .attr("height", height)
          .append("g")
          .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

        var arc_zero = d3.svg.arc()
          .innerRadius(radius - 0.20 * radius) // 0.45 * radius)
          .outerRadius(radius - 0.05 * radius) // 0.05 * radius)
          .startAngle(0);

        var path_zero = svg.append("path")
          .datum({
            endAngle: 2 * Math.PI
          })
          .attr("fill", color_empty)
          .attr("d", arc_zero);

          var progress_ratio = totalNumberOfVotes / total_votes;

          var arc = d3.svg.arc()
            .innerRadius(radius - 0.20 * radius) // 0.45 * radius)
            .outerRadius(radius - 0.05 * radius); // 0.05 * radius);

          var za_arc = svg.append("path")
            .datum({
              startAngle: 0,
              endAngle: 0
            })
            .attr("fill", color_rim)
            .attr("d", arc)
            .transition()
            .duration(2000)
            .attrTween("d", function (d) {
              var interpolate = d3.interpolate(d.endAngle, progress_ratio*2*Math.PI); //ratio_protiv * 2 * Math.PI);
              return function (t) {
                d.endAngle = interpolate(t);
                $('.votes-all').text(Math.floor(t*totalNumberOfVotes));
                return arc(d);
              }
            });

          // // TODO: Animated text that updates the total number of votes, but I don't know where to put it:-(
          // svg.append("text")
          //   .attr("dy", "0.5em") //.attr("dy", ".75em")
          //   .attr("y", 0) //.attr("y", -0.15 * radius)
          //   .attr("x", 0)
          //   .attr("text-anchor", "middle")
          //   .attr("font-family", "sans-serif")
          //   .attr("font-size", "22px")
          //   .attr("fill", color_rim)
          //   .text("0")
          //   .transition()
          //   .duration(2000)
          //   .tween("text",
          //     function () {
          //       var i = d3.interpolate(this.textContent, totalNumberOfVotes);
          //       return function (t) {
          //         this.textContent = Math.round(i(t)) + " glasova";
          //       };
          //     });

          svg.append("circle")
            .attr("cy", 0)
            .attr("cx", 0)
            .attr("r", radius - 0.20 * radius - 0.02 * radius)
            .attr("fill", color_center);

          svg.append("text")
            .attr("y", -0.05 * radius)
            .attr("x", 0)
            .attr("text-anchor", "middle")
            .attr("font-family", "sans-serif")
            .attr("font-size", "65px")
            .attr("fill", "white")
            .text("sabor");

          svg.append("text")
            .attr("y", 0.35 * radius)
            .attr("x", 0)
            .attr("text-anchor", "middle")
            .attr("font-family", "sans-serif")
            .attr("font-size", "50px")
            .attr("fill", "white")
            .text("2015.hr");

      });


    }};

}); 


sabor2015.directive('findRegion', function ($parse) {
  return {
    restrict: 'E',
    replace: false,
    link: function (scope, element, attrs) {

      $(element).html(
        '<input id="find-region-input" type="text">' + 
        '<div id="find-region-list" style="max-height:200px;overflow-y:scroll;width:500px;"></div>'
      );

      scope.loadElectionRegionsLocations(attrs.url);

      scope.$watch('election_regions_locations', function (newData, oldData) {

        if (!newData) { return; }
        
        var election_regions_locations = newData;

        // TODO: Some other options which are probably not needed at this time...
        // threshold: 0.6, location: 0, distance: 100, maxPatternLength: 32

        var options = {
            caseSensitive: false,
            includeScore: false,
            shouldSort: true,
            keys: ["location","county"] // TODO: Maybe is better to search just by location?
          };

        var fuse = new Fuse(election_regions_locations, options);

        $('#find-region-input').on('keyup', function(e) {
            $('#find-region-list').empty();
            var result = fuse.search($(this).val());

            result.forEach( function(d) {
                $('#find-region-list').append(
                  '<p style="margin-bottom:0px;">' + d.location + ', ' + d.county + ', ' + d.election_region + '. izborna jedinica</p>'
                  );
              });

        });


      });


    }};

}); 



sabor2015.directive('statisticsOfFriendsCircle', function ($parse) {
  return {
    restrict: 'E',
    replace: false,
    link: function (scope, element, attrs) {

     scope.$watchGroup(['election_regions','friends_data'], function (newData, oldData) {

        if (!newData[0] || !newData[1]) { return; }

        // var friends_data = newData[1];
        var friends_data = newData[1].data;
        var totalNumberOfVotes = newData[1].votes_count;
        var election_regions = newData[0];

        var data = friends_data.map(function(d){return {
          "id": d.list,
          "list": _.pluck(_.where(election_regions,{'list_id':Number(d.list)}),'short_name')[0],
          "votes": d.votes
        };});
        data.sort(function(a,b){return b.votes-a.votes;});

        // This could go outside watch but then we have problems with duplication of text labels...

        var margin = { top: 20, right: 20, bottom: 20, left: 20 },
            width = 900 - margin.left - margin.right;
        var height = 400 - margin.top - margin.bottom;
        var radius = Math.min(width, height) / 2;
        
        $(element).empty();

        var svg = d3.select(element[0]).append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g");

        svg.append("g")
          .attr("class", "slices");
        svg.append("g")
          .attr("class", "labels");
        svg.append("g")
          .attr("class", "lines");

        // ... up to here.

        var pie = d3.layout.pie()
          .sort(null)
          .padAngle(0.02)
          .value(function(d) {
            return d.votes;
          });

        var arc = d3.svg.arc()
          .outerRadius(radius * 0.7)
          .innerRadius(radius * 0.55);

        var outerArc = d3.svg.arc()
          .innerRadius(radius * 0.7)
          .outerRadius(radius * 0.7);

        svg.attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

        var key = function(d){ return d.data.list; };

        // All lists that together make less than 10% of friend's votes are aggregated to one category
        var reduceData = function(data) {
          // var totalNumberOfVotes = data.map(function(d){return d.votes})
          //       .reduce(function(prev,curr){return curr + prev;},0);
          var reducedData = [];
          var currentSumVotes = 0;
          var data_the_rest = {"list": "ostali", "votes": 0};
          data.forEach(function(d){
            if (currentSumVotes>= 0.9*totalNumberOfVotes) {
              data_the_rest.votes += d.votes;
            }
            else {
              reducedData.push(d);
            }
            currentSumVotes = currentSumVotes + d.votes;
          });
          if (data_the_rest.votes!=0) {
            reducedData.push(data_the_rest);
          }
          return reducedData;
        };

        var data = reduceData(data);

        // Pallete with sequential purple hues and colorfull pallete with specific color for each list
        var colors = "#f7f7f7"; // white for everyone
        if (attrs.colors=="sequential") {
          colors = ["#3f007d","#54278f","#6a51a3","#807dba","#9e9ac8","#bcbddc","#dadaeb","#efedf5","#fcfbfd","#fcfbfd","#fcfbfd","#fcfbfd","#fcfbfd","#fcfbfd"];
        } else if (attrs.colors=="colorfull") {
          colors = data.map(function(d){
            if (d.id==4) return "#d73027"; // red for SDP
            if (d.id==8) return "#4575b4"; // blue for HDZ
            if (d.id==3) return "#fee090"; // yellow for Zivi Zid
            if (d.id==6) return "#1a9850"; // green for Milan Bandic
            if (d.id==37) return "#f46d43"; // orange for MOST
            if (d.id==45) return "#a6d96a"; // light green for ORAH
            return "#f7f7f7"; // white for everyone else
          });
        }

        var color = d3.scale.ordinal()
          .domain( data.map(function(d){return d.list;}) )
          .range(colors);
        
        // if (newData[1].length==0) {
        if (totalNumberOfVotes<5) {
          var slice = svg.select(".slices").selectAll("path.slice")
            .data(pie([{ list: 'ostali', votes: 1 }]), key);
          slice.enter()
            .insert("path")
            .style("fill", "#f7f7f7")
            .attr("class", "slice")
            .attr("d",arc);
        }
        else {
          // Start with empty chart then visualize change to full chart
          changeAnimated([{ list: 'ostali', votes: 1 }],0);
          changeAnimated(data,4000);
        }

        function mergeWithFirstEqualZero(first, second){
          var secondSet = d3.set(); second.forEach(function(d) { secondSet.add(d.list); });

          var onlyFirst = first
            .filter(function(d){ return !secondSet.has(d.list) })
            .map(function(d) { return {list: d.list, votes: 0}; });
          return d3.merge([ second, onlyFirst ])
            .sort(function(a,b) {
              return d3.ascending(a.list, b.list);
            });
        }

        function changeAnimated(data,duration) {
          var data0 = svg.select(".slices").selectAll("path.slice")
            .data().map(function(d) { return d.data });
          if (data0.length == 0) data0 = data;
          var was = mergeWithFirstEqualZero(data, data0);
          var is = mergeWithFirstEqualZero(data0, data);

          /* ------- SLICE ARCS -------*/

          var slice = svg.select(".slices").selectAll("path.slice")
            .data(pie(was), key);

          slice.enter()
            .insert("path")
            .attr("class", "slice")
            .style("fill", function(d) { return color(d.data.list); })
            .each(function(d) {
              this._current = d;
            });

          slice = svg.select(".slices").selectAll("path.slice")
            .data(pie(is), key);

          slice   
            .transition().duration(duration)
            .attrTween("d", function(d) {
              var interpolate = d3.interpolate(this._current, d);
              var _this = this;
              return function(t) {
                _this._current = interpolate(t);
                return arc(_this._current);
              };
            });

          slice = svg.select(".slices").selectAll("path.slice")
            .data(pie(data), key);

          slice
            .exit().transition().delay(duration).duration(0)
            .remove();

          /* ------- TEXT LABELS -------*/

          var text = svg.select(".labels").selectAll("text")
            .data(pie(was), key);

          text.enter()
            .append("text")
            .attr("dy", ".35em")
            .style("opacity", 0)
            .text(function(d) {
              return d.data.list;
            })
            .each(function(d) {
              this._current = d;
            });
          
          function midAngle(d){
            return d.startAngle + (d.endAngle - d.startAngle)/2;
          }

          text = svg.select(".labels").selectAll("text")
            .data(pie(is), key);

          text.transition().duration(duration)
            .style("opacity", function(d) {
              return d.data.votes == 0 ? 0 : 1;
            })
            .attrTween("transform", function(d) {
              var interpolate = d3.interpolate(this._current, d);
              var _this = this;
              return function(t) {
                var d2 = interpolate(t);
                _this._current = d2;
                var pos = outerArc.centroid(d2);
                pos[0] = radius * (midAngle(d2) < Math.PI ? 1 : -1);
                return "translate("+ pos +")";
              };
            })
            .styleTween("text-anchor", function(d){
              var interpolate = d3.interpolate(this._current, d);
              return function(t) {
                var d2 = interpolate(t);
                return midAngle(d2) < Math.PI ? "start":"end";
              };
            })
            .tween("text",
              function (d) {
                var i = d3.interpolate(0, d.data.votes);
                return function (t) {
                  // this.textContent = d.data.list + ' (' + Math.ceil(i(t)) + ')';
                  this.textContent = d.data.list + ' (' + d3.format(".2f")((i(t)/totalNumberOfVotes)*100) + '%)';
                };
              });
          
          text = svg.select(".labels").selectAll("text")
            .data(pie(data), key);

          text
            .exit().transition().delay(duration)
            .remove();

          /* ------- SLICE TO TEXT POLYLINES -------*/

          var polyline = svg.select(".lines").selectAll("polyline")
            .data(pie(was), key);
          
          polyline.enter()
            .append("polyline")
            .style("opacity", 0)
            .each(function(d) {
              this._current = d;
            });

          polyline = svg.select(".lines").selectAll("polyline")
            .data(pie(is), key);
          
          polyline.transition().duration(duration)
            .style("opacity", function(d) {
              return d.data.votes == 0 ? 0 : .5;
            })
            .attrTween("points", function(d){
              this._current = this._current;
              var interpolate = d3.interpolate(this._current, d);
              var _this = this;
              return function(t) {
                var d2 = interpolate(t);
                _this._current = d2;
                var pos = outerArc.centroid(d2);
                pos[0] = radius * 0.95 * (midAngle(d2) < Math.PI ? 1 : -1);
                return [arc.centroid(d2), outerArc.centroid(d2), pos];
              };      
            });
          
          polyline = svg.select(".lines").selectAll("polyline")
            .data(pie(data), key);
          
          polyline
            .exit().transition().delay(duration)
            .remove();
        };


      });

    }};

}); 



sabor2015.directive('statisticsGlobalCircle', function ($parse) {
  return {
    restrict: 'E',
    replace: false,
    link: function (scope, element, attrs) {

     scope.$watchGroup(['election_regions','global_statistics'], function (newData, oldData) {

        if (!newData[0] || !newData[1]) { return; }

        // var friends_data = newData[1];
        var friends_data = newData[1].data;
        var totalNumberOfVotes = newData[1].votes_count;
        var election_regions = newData[0];

        var data = friends_data.map(function(d){return {
          "id": d.list,
          "list": _.pluck(_.where(election_regions,{'list_id':Number(d.list)}),'short_name')[0],
          "votes": d.votes
        };});
        data.sort(function(a,b){return b.votes-a.votes;});

        // This could go outside watch but then we have problems with duplication of text labels...

        var margin = { top: 20, right: 20, bottom: 20, left: 20 },
            width = 900 - margin.left - margin.right;
        var height = 400 - margin.top - margin.bottom;
        var radius = Math.min(width, height) / 2;
        
        $(element).empty();

        var svg = d3.select(element[0]).append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g");

        svg.append("g")
          .attr("class", "slices");
        svg.append("g")
          .attr("class", "labels");
        svg.append("g")
          .attr("class", "lines");

        // ... up to here.

        var pie = d3.layout.pie()
          .sort(null)
          .padAngle(0.02)
          .value(function(d) {
            return d.votes;
          });

        var arc = d3.svg.arc()
          .outerRadius(radius * 0.7)
          .innerRadius(radius * 0.55);

        var outerArc = d3.svg.arc()
          .innerRadius(radius * 0.7)
          .outerRadius(radius * 0.7);

        svg.attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

        var key = function(d){ return d.data.list; };

        // All lists that together make less than 10% of friend's votes are aggregated to one category
        var reduceData = function(data) {
          // var totalNumberOfVotes = data.map(function(d){return d.votes})
          //       .reduce(function(prev,curr){return curr + prev;},0);
          var reducedData = [];
          var currentSumVotes = 0;
          var data_the_rest = {"list": "ostali", "votes": 0};
          data.forEach(function(d){
            if (currentSumVotes>= 0.9*totalNumberOfVotes) {
              data_the_rest.votes += d.votes;
            }
            else {
              reducedData.push(d);
            }
            currentSumVotes = currentSumVotes + d.votes;
          });
          if (data_the_rest.votes!=0) {
            reducedData.push(data_the_rest);
          }
          return reducedData;
        };

        var data = reduceData(data);

        // Pallete with sequential purple hues and colorfull pallete with specific color for each list
        var colors = "#f7f7f7"; // white for everyone
        if (attrs.colors=="sequential") {
          colors = ["#3f007d","#54278f","#6a51a3","#807dba","#9e9ac8","#bcbddc","#dadaeb","#efedf5","#fcfbfd","#fcfbfd","#fcfbfd","#fcfbfd","#fcfbfd","#fcfbfd"];
        } else if (attrs.colors=="colorfull") {
          colors = data.map(function(d){
            if (d.id==4) return "#d73027"; // red for SDP
            if (d.id==8) return "#4575b4"; // blue for HDZ
            if (d.id==3) return "#fee090"; // yellow for Zivi Zid
            if (d.id==6) return "#1a9850"; // green for Milan Bandic
            if (d.id==37) return "#f46d43"; // orange for MOST
            if (d.id==45) return "#a6d96a"; // light green for ORAH
            return "#f7f7f7"; // white for everyone else
          });
        }

        var color = d3.scale.ordinal()
          .domain( data.map(function(d){return d.list;}) )
          .range(colors);
        
        // if (newData[1].length==0) {
        if (totalNumberOfVotes<5) {
          var slice = svg.select(".slices").selectAll("path.slice")
            .data(pie([{ list: 'ostali', votes: 1 }]), key);
          slice.enter()
            .insert("path")
            .style("fill", "#f7f7f7")
            .attr("class", "slice")
            .attr("d",arc);
        }
        else {
          // Start with empty chart then visualize change to full chart
          changeAnimated([{ list: 'ostali', votes: 1 }],0);
          changeAnimated(data,4000);
        }

        function mergeWithFirstEqualZero(first, second){
          var secondSet = d3.set(); second.forEach(function(d) { secondSet.add(d.list); });

          var onlyFirst = first
            .filter(function(d){ return !secondSet.has(d.list) })
            .map(function(d) { return {list: d.list, votes: 0}; });
          return d3.merge([ second, onlyFirst ])
            .sort(function(a,b) {
              return d3.ascending(a.list, b.list);
            });
        }

        function changeAnimated(data,duration) {
          var data0 = svg.select(".slices").selectAll("path.slice")
            .data().map(function(d) { return d.data });
          if (data0.length == 0) data0 = data;
          var was = mergeWithFirstEqualZero(data, data0);
          var is = mergeWithFirstEqualZero(data0, data);

          /* ------- SLICE ARCS -------*/

          var slice = svg.select(".slices").selectAll("path.slice")
            .data(pie(was), key);

          slice.enter()
            .insert("path")
            .attr("class", "slice")
            .style("fill", function(d) { return color(d.data.list); })
            .each(function(d) {
              this._current = d;
            });

          slice = svg.select(".slices").selectAll("path.slice")
            .data(pie(is), key);

          slice   
            .transition().duration(duration)
            .attrTween("d", function(d) {
              var interpolate = d3.interpolate(this._current, d);
              var _this = this;
              return function(t) {
                _this._current = interpolate(t);
                return arc(_this._current);
              };
            });

          slice = svg.select(".slices").selectAll("path.slice")
            .data(pie(data), key);

          slice
            .exit().transition().delay(duration).duration(0)
            .remove();

          /* ------- TEXT LABELS -------*/

          var text = svg.select(".labels").selectAll("text")
            .data(pie(was), key);

          text.enter()
            .append("text")
            .attr("dy", ".35em")
            .style("opacity", 0)
            .text(function(d) {
              return d.data.list;
            })
            .each(function(d) {
              this._current = d;
            });
          
          function midAngle(d){
            return d.startAngle + (d.endAngle - d.startAngle)/2;
          }

          text = svg.select(".labels").selectAll("text")
            .data(pie(is), key);

          text.transition().duration(duration)
            .style("opacity", function(d) {
              return d.data.votes == 0 ? 0 : 1;
            })
            .attrTween("transform", function(d) {
              var interpolate = d3.interpolate(this._current, d);
              var _this = this;
              return function(t) {
                var d2 = interpolate(t);
                _this._current = d2;
                var pos = outerArc.centroid(d2);
                pos[0] = radius * (midAngle(d2) < Math.PI ? 1 : -1);
                return "translate("+ pos +")";
              };
            })
            .styleTween("text-anchor", function(d){
              var interpolate = d3.interpolate(this._current, d);
              return function(t) {
                var d2 = interpolate(t);
                return midAngle(d2) < Math.PI ? "start":"end";
              };
            })
            .tween("text",
              function (d) {
                var i = d3.interpolate(0, d.data.votes);
                return function (t) {
                  // this.textContent = d.data.list + ' (' + Math.ceil(i(t)) + ')';
                  this.textContent = d.data.list + ' (' + d3.format(".2f")((i(t)/totalNumberOfVotes)*100) + '%)';
                };
              });
          
          text = svg.select(".labels").selectAll("text")
            .data(pie(data), key);

          text
            .exit().transition().delay(duration)
            .remove();

          /* ------- SLICE TO TEXT POLYLINES -------*/

          var polyline = svg.select(".lines").selectAll("polyline")
            .data(pie(was), key);
          
          polyline.enter()
            .append("polyline")
            .style("opacity", 0)
            .each(function(d) {
              this._current = d;
            });

          polyline = svg.select(".lines").selectAll("polyline")
            .data(pie(is), key);
          
          polyline.transition().duration(duration)
            .style("opacity", function(d) {
              return d.data.votes == 0 ? 0 : .5;
            })
            .attrTween("points", function(d){
              this._current = this._current;
              var interpolate = d3.interpolate(this._current, d);
              var _this = this;
              return function(t) {
                var d2 = interpolate(t);
                _this._current = d2;
                var pos = outerArc.centroid(d2);
                pos[0] = radius * 0.95 * (midAngle(d2) < Math.PI ? 1 : -1);
                return [arc.centroid(d2), outerArc.centroid(d2), pos];
              };      
            });
          
          polyline = svg.select(".lines").selectAll("polyline")
            .data(pie(data), key);
          
          polyline
            .exit().transition().delay(duration)
            .remove();
        };


      });

    }};

}); 



sabor2015.directive('statisticsRegionCircle', function ($parse) {
  return {
    restrict: 'E',
    replace: false,
    link: function (scope, element, attrs) {

     scope.$watchGroup(['election_regions','region_statistics'], function (newData, oldData) {

        if (!newData[0] || !newData[1]) { return; }

        // var friends_data = newData[1];
        var friends_data = newData[1].data;
        var totalNumberOfVotes = newData[1].votes_count;
        var election_regions = newData[0];

        var data = friends_data.map(function(d){return {
          "id": d.list,
          "list": _.pluck(_.where(election_regions,{'list_id':Number(d.list)}),'short_name')[0],
          "votes": d.votes
        };});
        data.sort(function(a,b){return b.votes-a.votes;});

        // This could go outside watch but then we have problems with duplication of text labels...

        var margin = { top: 20, right: 20, bottom: 20, left: 20 },
            width = 900 - margin.left - margin.right;
        var height = 400 - margin.top - margin.bottom;
        var radius = Math.min(width, height) / 2;
        
        $(element).empty();

        var svg = d3.select(element[0]).append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g");

        svg.append("g")
          .attr("class", "slices");
        svg.append("g")
          .attr("class", "labels");
        svg.append("g")
          .attr("class", "lines");

        // ... up to here.

        var pie = d3.layout.pie()
          .sort(null)
          .padAngle(0.02)
          .value(function(d) {
            return d.votes;
          });

        var arc = d3.svg.arc()
          .outerRadius(radius * 0.7)
          .innerRadius(radius * 0.55);

        var outerArc = d3.svg.arc()
          .innerRadius(radius * 0.7)
          .outerRadius(radius * 0.7);

        svg.attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

        var key = function(d){ return d.data.list; };

        // All lists that together make less than 10% of friend's votes are aggregated to one category
        var reduceData = function(data) {
          // var totalNumberOfVotes = data.map(function(d){return d.votes})
          //       .reduce(function(prev,curr){return curr + prev;},0);
          var reducedData = [];
          var currentSumVotes = 0;
          var data_the_rest = {"list": "ostali", "votes": 0};
          data.forEach(function(d){
            if (currentSumVotes>= 0.9*totalNumberOfVotes) {
              data_the_rest.votes += d.votes;
            }
            else {
              reducedData.push(d);
            }
            currentSumVotes = currentSumVotes + d.votes;
          });
          if (data_the_rest.votes!=0) {
            reducedData.push(data_the_rest);
          }
          return reducedData;
        };

        var data = reduceData(data);

        // Pallete with sequential purple hues and colorfull pallete with specific color for each list
        var colors = "#f7f7f7"; // white for everyone
        if (attrs.colors=="sequential") {
          colors = ["#3f007d","#54278f","#6a51a3","#807dba","#9e9ac8","#bcbddc","#dadaeb","#efedf5","#fcfbfd","#fcfbfd","#fcfbfd","#fcfbfd","#fcfbfd","#fcfbfd"];
        } else if (attrs.colors=="colorfull") {
          colors = data.map(function(d){
            if (d.id==4) return "#d73027"; // red for SDP
            if (d.id==8) return "#4575b4"; // blue for HDZ
            if (d.id==3) return "#fee090"; // yellow for Zivi Zid
            if (d.id==6) return "#1a9850"; // green for Milan Bandic
            if (d.id==37) return "#f46d43"; // orange for MOST
            if (d.id==45) return "#a6d96a"; // light green for ORAH
            return "#f7f7f7"; // white for everyone else
          });
        }

        var color = d3.scale.ordinal()
          .domain( data.map(function(d){return d.list;}) )
          .range(colors);
        
        // if (newData[1].length==0) {
        if (totalNumberOfVotes<5) {
          var slice = svg.select(".slices").selectAll("path.slice")
            .data(pie([{ list: 'ostali', votes: 1 }]), key);
          slice.enter()
            .insert("path")
            .style("fill", "#f7f7f7")
            .attr("class", "slice")
            .attr("d",arc);
        }
        else {
          // Start with empty chart then visualize change to full chart
          changeAnimated([{ list: 'ostali', votes: 1 }],0);
          changeAnimated(data,4000);
        }

        function mergeWithFirstEqualZero(first, second){
          var secondSet = d3.set(); second.forEach(function(d) { secondSet.add(d.list); });

          var onlyFirst = first
            .filter(function(d){ return !secondSet.has(d.list) })
            .map(function(d) { return {list: d.list, votes: 0}; });
          return d3.merge([ second, onlyFirst ])
            .sort(function(a,b) {
              return d3.ascending(a.list, b.list);
            });
        }

        function changeAnimated(data,duration) {
          var data0 = svg.select(".slices").selectAll("path.slice")
            .data().map(function(d) { return d.data });
          if (data0.length == 0) data0 = data;
          var was = mergeWithFirstEqualZero(data, data0);
          var is = mergeWithFirstEqualZero(data0, data);

          /* ------- SLICE ARCS -------*/

          var slice = svg.select(".slices").selectAll("path.slice")
            .data(pie(was), key);

          slice.enter()
            .insert("path")
            .attr("class", "slice")
            .style("fill", function(d) { return color(d.data.list); })
            .each(function(d) {
              this._current = d;
            });

          slice = svg.select(".slices").selectAll("path.slice")
            .data(pie(is), key);

          slice   
            .transition().duration(duration)
            .attrTween("d", function(d) {
              var interpolate = d3.interpolate(this._current, d);
              var _this = this;
              return function(t) {
                _this._current = interpolate(t);
                return arc(_this._current);
              };
            });

          slice = svg.select(".slices").selectAll("path.slice")
            .data(pie(data), key);

          slice
            .exit().transition().delay(duration).duration(0)
            .remove();

          /* ------- TEXT LABELS -------*/

          var text = svg.select(".labels").selectAll("text")
            .data(pie(was), key);

          text.enter()
            .append("text")
            .attr("dy", ".35em")
            .style("opacity", 0)
            .text(function(d) {
              return d.data.list;
            })
            .each(function(d) {
              this._current = d;
            });
          
          function midAngle(d){
            return d.startAngle + (d.endAngle - d.startAngle)/2;
          }

          text = svg.select(".labels").selectAll("text")
            .data(pie(is), key);

          text.transition().duration(duration)
            .style("opacity", function(d) {
              return d.data.votes == 0 ? 0 : 1;
            })
            .attrTween("transform", function(d) {
              var interpolate = d3.interpolate(this._current, d);
              var _this = this;
              return function(t) {
                var d2 = interpolate(t);
                _this._current = d2;
                var pos = outerArc.centroid(d2);
                pos[0] = radius * (midAngle(d2) < Math.PI ? 1 : -1);
                return "translate("+ pos +")";
              };
            })
            .styleTween("text-anchor", function(d){
              var interpolate = d3.interpolate(this._current, d);
              return function(t) {
                var d2 = interpolate(t);
                return midAngle(d2) < Math.PI ? "start":"end";
              };
            })
            .tween("text",
              function (d) {
                var i = d3.interpolate(0, d.data.votes);
                return function (t) {
                  // this.textContent = d.data.list + ' (' + Math.ceil(i(t)) + ')';
                  this.textContent = d.data.list + ' (' + d3.format(".2f")((i(t)/totalNumberOfVotes)*100) + '%)';
                };
              });
          
          text = svg.select(".labels").selectAll("text")
            .data(pie(data), key);

          text
            .exit().transition().delay(duration)
            .remove();

          /* ------- SLICE TO TEXT POLYLINES -------*/

          var polyline = svg.select(".lines").selectAll("polyline")
            .data(pie(was), key);
          
          polyline.enter()
            .append("polyline")
            .style("opacity", 0)
            .each(function(d) {
              this._current = d;
            });

          polyline = svg.select(".lines").selectAll("polyline")
            .data(pie(is), key);
          
          polyline.transition().duration(duration)
            .style("opacity", function(d) {
              return d.data.votes == 0 ? 0 : .5;
            })
            .attrTween("points", function(d){
              this._current = this._current;
              var interpolate = d3.interpolate(this._current, d);
              var _this = this;
              return function(t) {
                var d2 = interpolate(t);
                _this._current = d2;
                var pos = outerArc.centroid(d2);
                pos[0] = radius * 0.95 * (midAngle(d2) < Math.PI ? 1 : -1);
                return [arc.centroid(d2), outerArc.centroid(d2), pos];
              };      
            });
          
          polyline = svg.select(".lines").selectAll("polyline")
            .data(pie(data), key);
          
          polyline
            .exit().transition().delay(duration)
            .remove();
        };


      });

    }};

}); 
