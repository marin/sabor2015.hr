DROP TABLE IF EXISTS county CASCADE;

CREATE TABLE county (
    id SERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(255) NOT NULL UNIQUE
);


DROP TABLE IF EXISTS location CASCADE;

CREATE TABLE location (
    id BIGINT NOT NULL PRIMARY KEY,
    city VARCHAR(255) NOT NULL,
    zip VARCHAR(100),
    county_id INT REFERENCES county(id),
    country VARCHAR(255) NOT NULL,
    latitude REAL NOT NULL,
    longitude REAL NOT NULL,
    geo_data JSONB
);


DROP TABLE IF EXISTS facebook_user CASCADE;

CREATE TABLE facebook_user (
    id BIGINT NOT NULL PRIMARY KEY,
    gender CHAR(1),
    birthday_data VARCHAR(16),
    birthday DATE,
    friend_count INT,
    hometown_id BIGINT REFERENCES location(id) ON DELETE SET NULL,
    current_location_id BIGINT REFERENCES location(id) ON DELETE SET NULL,
    friends BIGINT[],
    created_on TIMESTAMPTZ NOT NULL DEFAULT now(),
    modified_on TIMESTAMPTZ NOT NULL
);


DROP TABLE IF EXISTS friendship CASCADE;

CREATE TABLE friendship (
    from_id BIGINT NOT NULL,
    to_id   BIGINT NOT NULL,
    PRIMARY KEY (from_id, to_id),
    UNIQUE (to_id, from_id)
);


DROP TABLE IF EXISTS user_session CASCADE;

CREATE TABLE user_session (
    token_id UUID NOT NULL PRIMARY KEY,
    referer_token_id UUID REFERENCES user_session(token_id),
    referer_token_type CHAR(2),
    referer TEXT,
    created_on TIMESTAMPTZ NOT NULL DEFAULT now()
);

CREATE INDEX ix_user_session_referer_token ON user_session(referer_token_id);


DROP TABLE IF EXISTS user_login CASCADE;

CREATE TABLE user_login (
    token_id UUID NOT NULL PRIMARY KEY REFERENCES user_session(token_id),
    user_id BIGINT NOT NULL,
    created_on TIMESTAMPTZ NOT NULL DEFAULT now()
);

CREATE INDEX ix_user_login_user ON user_login(user_id);


DROP TABLE IF EXISTS user_share CASCADE;

CREATE TABLE user_share (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    token_id UUID NOT NULL REFERENCES user_session(token_id),
    share_type CHAR(2) NOT NULL,
    shared_on TIMESTAMPTZ NOT NULL DEFAULT now()
);

CREATE INDEX ix_user_share_token ON user_share(token_id);


DROP TABLE IF EXISTS region CASCADE;

CREATE TABLE region (
    id INT NOT NULL PRIMARY KEY,
    name VARCHAR(256) NOT NULL
);

DROP TABLE IF EXISTS party CASCADE;

CREATE TABLE party(
    id INT NOT NULL PRIMARY KEY,
    name VARCHAR(256) NOT NULL
);

DROP TABLE IF EXISTS election_list CASCADE;

CREATE TABLE election_list (
    id INT NOT NULL PRIMARY KEY,
    name VARCHAR(256) NOT NULL,
    description TEXT NOT NULL
);

DROP TABLE IF EXISTS election_list_regions CASCADE;

CREATE TABLE election_list_regions (
    election_list_id INT NOT NULL REFERENCES election_list(id),
    region_id INT NOT NULL REFERENCES region(id),
    PRIMARY KEY (election_list_id, region_id)
);

CREATE UNIQUE INDEX ix_election_list_regions_region ON election_list_regions(region_id, election_list_id);

DROP TABLE IF EXISTS vote CASCADE;

CREATE TABLE vote (
    user_id BIGINT NOT NULL PRIMARY KEY REFERENCES facebook_user(id),
    election_list_id INT NOT NULL REFERENCES election_list(id),
    region_id INT NOT NULL REFERENCES region(id),
    percentage SMALLINT NOT NULL CHECK (percentage BETWEEN 0 AND 100),
    preferences SMALLINT[],
    voted_on TIMESTAMPTZ NOT NULL DEFAULT now()
);

DROP TABLE IF EXISTS vote_history CASCADE;

CREATE TABLE vote_history (
    user_id BIGINT NOT NULL REFERENCES facebook_user(id),
    voted_on TIMESTAMPTZ NOT NULL DEFAULT now(),
    election_list_id INT NOT NULL REFERENCES election_list(id),
    region_id INT NOT NULL REFERENCES region(id),
    percentage SMALLINT NOT NULL CHECK (percentage BETWEEN 0 AND 100),
    preferences SMALLINT[],
    PRIMARY KEY (user_id, voted_on)
);

DROP TRIGGER IF EXISTS vote_history_audit_trigger ON vote;

DROP FUNCTION IF EXISTS vote_history_audit();

CREATE OR REPLACE FUNCTION vote_history_audit()
RETURNS TRIGGER AS $$
BEGIN
    INSERT INTO vote_history(
            user_id, voted_on, election_list_id, region_id, percentage, preferences)
    VALUES (NEW.user_id, NEW.voted_on, NEW.election_list_id,
            NEW.region_id, NEW.percentage, NEW.preferences);
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER vote_history_audit_trigger
	AFTER INSERT OR UPDATE ON vote
	FOR EACH ROW EXECUTE PROCEDURE vote_history_audit();


DROP FUNCTION IF EXISTS rebuild_friendships(userId BIGINT);

CREATE OR REPLACE FUNCTION rebuild_friendships(userId BIGINT) RETURNS VOID AS $$
DECLARE
    result INT;
BEGIN
    WITH existing_friendships AS (
        SELECT from_id, to_id
        FROM friendship
        WHERE from_id = userId
        OR to_id = userId
    ),
    new_friendships AS (
        SELECT f.id AS from_id, unnest(f.friends) AS to_id
        FROM facebook_user f
        WHERE f.id = userId

        UNION

        SELECT unnest(f.friends) AS from_id, f.id AS to_id
        FROM facebook_user f
        WHERE f.id = userId
    ),
    delete_list AS (
        SELECT *
        FROM existing_friendships ef
        WHERE (ef.from_id, ef.to_id) NOT IN (SELECT from_id, to_id FROM new_friendships)
    ),
    add_list AS (
        SELECT *
        FROM new_friendships nf
        WHERE (nf.from_id, nf.to_id) NOT IN (SELECT from_id, to_id FROM existing_friendships)
    ),
    delete_items AS (
        DELETE FROM friendship AS f
        USING delete_list dl
        WHERE (f.from_id, f.to_id) = (dl.from_id, dl.to_id)
        RETURNING f.from_id, f.to_id
    ),
    add_items AS (
        INSERT INTO friendship(from_id, to_id)
        (SELECT from_id, to_id FROM add_list)
        RETURNING from_id, to_id
    )
    SELECT 1
    INTO result;
END;
$$ LANGUAGE plpgsql;


DROP MATERIALIZED VIEW IF EXISTS total_reach CASCADE;

CREATE MATERIALIZED VIEW total_reach AS (

    WITH RECURSIVE refered_users AS (
        SELECT
            u.user_id,
            u.user_id    AS new_user_id,
            u.created_on,
            NULL :: UUID AS parent_token_id,
            u.token_id
        FROM user_login u

        UNION ALL

        SELECT
            ru.user_id,
            ul.user_id AS new_user_id,
            ul.created_on,
            ru.token_id,
            us.token_id
        FROM refered_users ru
            INNER JOIN user_session us ON us.referer_token_id = ru.token_id
            INNER JOIN user_login ul ON ul.token_id = us.token_id
    ),
    dictinct_connections AS (
        SELECT DISTINCT
            ru.user_id,
            ru.new_user_id
        FROM refered_users ru

        )
    SELECT
        u.id AS user_id,
        count(dc.user_id) as reach_count
    FROM facebook_user u
    LEFT OUTER JOIN dictinct_connections dc ON dc.user_id = u.id
    GROUP BY u.id
);

CREATE UNIQUE INDEX ix_total_reach ON total_reach(user_id);


DROP MATERIALIZED VIEW IF EXISTS reach CASCADE;

CREATE MATERIALIZED VIEW reach AS (

    WITH refered_users AS (
        SELECT u.user_id, ul.user_id as new_user_id
        FROM user_login u
        INNER JOIN user_session us ON us.referer_token_id = u.token_id
        INNER JOIN user_login ul ON ul.token_id = us.token_id

        UNION ALL

        SELECT u.user_id, u.user_id as new_user_id
        FROM user_login u
    ),
    dictinct_connections AS (
        SELECT DISTINCT ru.user_id, ru.new_user_id
        FROM refered_users ru
    ),
    user_counts AS (
	SELECT u.id AS user_id, count(dc.user_id)
	FROM facebook_user u
	LEFT OUTER JOIN dictinct_connections dc ON dc.user_id = u.id
	GROUP BY u.id
    )
    SELECT uc.user_id, uc.count, rank() OVER (ORDER BY uc.count DESC) as rank
    FROM user_counts uc
);

CREATE UNIQUE INDEX ix_reach ON reach(user_id);


DROP MATERIALIZED VIEW IF EXISTS friend_votes CASCADE;

CREATE MATERIALIZED VIEW friend_votes AS (

    SELECT d.id AS user_id, array_to_json(array_agg(d.agg_vote)) AS aggregated_votes
    FROM (
        SELECT u.id, array_to_json(ARRAY[v.election_list_id, count(*)]) AS agg_vote
        FROM facebook_user u
        INNER JOIN friendship f ON f.from_id = u.id
        INNER JOIN vote v ON v.user_id = f.to_id
        GROUP BY u.id, v.election_list_id
        ORDER BY u.id, v.election_list_id
        ) d
    GROUP BY d.id
);

CREATE UNIQUE INDEX ix_friend_votes ON friend_votes(user_id);


DROP MATERIALIZED VIEW IF EXISTS global_votes CASCADE;

CREATE MATERIALIZED VIEW global_votes AS (

    SELECT 1 AS id, array_to_json(array_agg(d.agg_vote)) AS aggregated_votes
    FROM (
        SELECT array_to_json(ARRAY[el.id, count(v.election_list_id)]) AS agg_vote
        FROM election_list el
        LEFT OUTER JOIN vote v ON el.id = v.election_list_id
        GROUP BY el.id
        ORDER BY el.id
    ) d
);

CREATE UNIQUE INDEX ix_global_votes ON global_votes(id);


DROP MATERIALIZED VIEW IF EXISTS region_votes CASCADE;

CREATE MATERIALIZED VIEW region_votes AS (

    SELECT d.region_id, array_to_json(array_agg(d.agg_vote)) AS aggregated_votes
    FROM (
        SELECT elr.region_id, array_to_json(ARRAY[el.id, count(v.election_list_id)]) AS agg_vote
        FROM election_list el
        INNER JOIN election_list_regions elr ON elr.election_list_id = el.id
        LEFT OUTER JOIN vote v ON v.election_list_id = el.id AND  v.region_id = elr.region_id
        GROUP BY elr.region_id, el.id
        ORDER BY elr.region_id
    ) d
    GROUP BY d.region_id
);

CREATE UNIQUE INDEX ix_region_votes ON region_votes(region_id);


DROP MATERIALIZED VIEW IF EXISTS vote_in_time CASCADE;

CREATE MATERIALIZED VIEW vote_in_time AS (

	WITH ordered_votes AS (
		SELECT voted_on, rank() OVER (PARTITION BY user_id ORDER BY voted_on ASC) as rank
		FROM vote_history
	),
	first_votes AS (
		SELECT voted_on
		FROM ordered_votes
		WHERE rank = 1
	)
	SELECT tm AS date_hour, count(fv.voted_on) AS votes
	FROM generate_series('2015-11-02 00:00:00+0100'::TIMESTAMPTZ, '2015-11-07 23:00:00+0100'::TIMESTAMPTZ, INTERVAL '1 hour') tm
	LEFT OUTER JOIN first_votes fv ON fv.voted_on BETWEEN tm AND tm + INTERVAL '1 hour'
	GROUP BY 1
	ORDER BY 1 ASC
);

CREATE UNIQUE INDEX ix_votes_in_time ON vote_in_time(date_hour);


DROP MATERIALIZED VIEW IF EXISTS user_rank;

CREATE MATERIALIZED VIEW user_rank AS (
    WITH friend_counts AS (
        SELECT u.id, count(f.from_id)
        FROM facebook_user u
        LEFT OUTER JOIN friendship f ON f.from_id = u.id
        GROUP BY u.id
    )
    SELECT id, count AS friend_count, rank() OVER (ORDER BY count DESC) AS rank
    FROM friend_counts
);

CREATE UNIQUE INDEX ix_user_rank ON user_rank(id);


--
-- TOC entry 2078 (class 0 OID 22832)
-- Dependencies: 199
-- Data for Name: election_list; Type: TABLE DATA; Schema: public; Owner: sabor2015
--

INSERT INTO election_list (id, name, description) VALUES (1, 'AKCIJA MLADIH - AM', 'AKCIJA MLADIH - AM');
INSERT INTO election_list (id, name, description) VALUES (2, 'ŽIVI ZID, AM', 'ŽIVI ZID, AKCIJA MLADIH - AM');
INSERT INTO election_list (id, name, description) VALUES (3, 'ŽIVI ZID', 'ŽIVI ZID');
INSERT INTO election_list (id, name, description) VALUES (4, 'SDP, HNS, HSU, Laburisti, [...]', 'SOCIJALDEMOKRATSKA PARTIJA HRVATSKE - SDP, HRVATSKA NARODNA STRANKA - LIBERALNI DEMOKRATI - HNS, HRVATSKA STRANKA UMIROVLJENIKA - HSU, HRVATSKI LABURISTI - STRANKA RADA, AUTOHTONA - HRVATSKA SELJAČKA STRANKA - A - HSS, ZAGORSKA STRANKA - ZS, SAMOSTALNA DEMOKRATSKA SRPSKA STRANKA - SDSS (IX.)');
INSERT INTO election_list (id, name, description) VALUES (5, 'A-HSP', 'AUTOHTONA - HRVATSKA STRANKA PRAVA - A-HSP');
INSERT INTO election_list (id, name, description) VALUES (6, 'BANDIĆ 365, DPS, DSŽ, [...]', 'BANDIĆ MILAN 365 - STRANKA RADA I SOLIDARNOSTI, DEMOKRATSKA PRIGORSKO - ZAGREBAČKA STRANKA - DPS, DEMOKRATSKA STRANKA ŽENA - DSŽ, HRVATSKA EUROPSKA STRANKA - HES, HRVATSKA RADNIČKA STRANKA - HRS, HRVATSKA STRANKA ZELENIH - EKO SAVEZ - ZELENI ,ISTARSKI DEMOKRATI - DEMOCRATICI ISTRIANI - ID - DI ,MEĐIMURSKA STRANKA - MS, NEZAVISNI SELJACI HRVATSKE - NSH, NOVI VAL - STRANKA RAZVOJA - NOVI VAL, STRANKA UMIROVLJENIKA - SU, UMIROVLJENIČKA DEMOKRATSKA UNIJA - UDU, ZELENI SAVEZ - ZELENI, ZELENA STRANKA - ZS');
INSERT INTO election_list (id, name, description) VALUES (7, 'Lista Muhameda Zahirovića', 'AUSTRIJSKA BUGARSKA NJEMAČKA POLJSKA ROMSKA RUMUNJSKA RUSINSKA RUSKA TURSKA UKRAJINSKA VLAŠKA I ŽIDOVSKA NACIONALNA MANJINA, MUHAMED ZAHIROVIĆ (kandidat), ''BANDIĆ MILAN 365 - STRANKA RADA I SOLIDARNOSTI'', DEMOKRATSKA PRIGORSKO - ZAGREBAČKA STRANKA - DPS, DEMOKRATSKA STRANKA ŽENA - DSŽ, HRVATSKA EUROPSKA STRANKA - HES, HRVATSKA RADNIČKA STRANKA - HRS, HRVATSKA STRANKA ZELENIH - EKO SAVEZ - ZELENI ,ISTARSKI DEMOKRATI - DEMOCRATICI ISTRIANI - ID - DI ,MEĐIMURSKA STRANKA - MS, NEZAVISNI SELJACI HRVATSKE - NSH, NOVI VAL - STRANKA RAZVOJA - NOVI VAL, STRANKA UMIROVLJENIKA - SU, UMIROVLJENIČKA DEMOKRATSKA UNIJA - UDU, ZELENI SAVEZ - ZELENI, ZELENA STRANKA - ZS');
INSERT INTO election_list (id, name, description) VALUES (8, 'HDZ, HSS, HSP AS, BUZ, [...]', 'HRVATSKA DEMOKRATSKA ZAJEDNICA - HDZ, HRVATSKA SELJAČKA STRANKA - HSS, HRVATSKA STRANKA PRAVA DR. ANTE STARČEVIĆ - HSP AS, BLOK UMIROVLJENICI ZAJEDNO - BUZ, HRVATSKA SOCIJALNO - LIBERALNA STRANKA - HSLS, HRAST - POKRET ZA USPJEŠNU HRVATSKU, HRVATSKA DEMOKRŠĆANSKA STRANKA - HDS, ZAGORSKA DEMOKRATSKA STRANKA - ZDS ');
INSERT INTO election_list (id, name, description) VALUES (9, 'Lista Nedžada Hodžića', 'ALBANSKA BOŠNJAČKA CRNOGORSKA MAKEDONSKA I SLOVENSKA NACIONALNA MANJINA, NEDŽAD HODŽIĆ (kandidat), BOŠNJAČKA DEMOKRATSKA STRANKA HRVATSKE - BDSH');
INSERT INTO election_list (id, name, description) VALUES (10, 'Lista Senada Pršića', 'ALBANSKA BOŠNJAČKA CRNOGORSKA MAKEDONSKA I SLOVENSKA NACIONALNA MANJINA, SENAD PRŠIĆ (kandidat), BOŠNJAČKA NACIONALNA ZAJEDNICA HRVATSKE');
INSERT INTO election_list (id, name, description) VALUES (11, 'Lista Alena Džombe', 'ALBANSKA BOŠNJAČKA CRNOGORSKA MAKEDONSKA I SLOVENSKA NACIONALNA MANJINA, ALEN DŽOMBA (kandidat), NAPRIJED HRVATSKA! - PROGRESIVNI SAVEZ IVE JOSIPOVIĆA, UDRUGA BOŠNJAKA BRANITELJA DOMOVINSKOG RATA VUKOVARSKO - SRIJEMSKE ŽUPANIJE, BOŠNJAČKO KULTURNO UMJETNIČKO DRUŠTVO ''BEHAR'' GUNJA');
INSERT INTO election_list (id, name, description) VALUES (12, 'BDSH', 'BRANITELJSKO DOMOLJUBNA STRANKA HRVATSKE');
INSERT INTO election_list (id, name, description) VALUES (13, 'Lista Roberta Jankovicsa', 'MAĐARSKA NACIONALNA MANJINA, RÓBERT JANKOVICS (kandidat), ZOLTAN BALAŽ-PIRI (zamjenik), DEMOKRATSKA ZAJEDNICA MAĐARA HRVATSKE - DZMH - HORVÁTORSZÁGI MAGYAROK DEMOKRATIKUS KÖZÖSSÉGE - HMDK');
INSERT INTO election_list (id, name, description) VALUES (14, 'DESNO', 'DEMOKRATSKI SAVEZ NACIONALNE OBNOVE - DESNO');
INSERT INTO election_list (id, name, description) VALUES (15, 'NAPRIJED HRVATSKA!, [...]', 'NAPRIJED HRVATSKA! - PROGRESIVNI SAVEZ IVE JOSIPOVIĆA, NARODNA STRANKA - REFORMISTI - REFORMISTI, STRANKA HRVATSKIH UMIROVLJENIKA - UMIROVLJENICI, ZELENI FORUM, DUBROVAČKI DEMOKRATSKI SABOR - DDS');
INSERT INTO election_list (id, name, description) VALUES (16, 'Lista Ermine Lekaj Prljaskaj', 'ALBANSKA BOŠNJAČKA CRNOGORSKA MAKEDONSKA I SLOVENSKA NACIONALNA MANJINA, ERMINA LEKAJ PRLJASKAJ (kandidatkinja), FORUM ALBANSKIH INTELEKTUALACA - FAI, KULTURNA UDRUGA ALBANSKE NACIONALNE MANJINE ZADAR - KUANMZ, ZAVIČAJNA UDRUGA ''HASI'' - HASI, UDRUGA KOSOVARA U REPUBLICI HRVATSKOJ - KOSOVA-RH');
INSERT INTO election_list (id, name, description) VALUES (17, 'HKDU, HDS, ND', 'HRVATSKA KRŠĆANSKA DEMOKRATSKA UNIJA - HKDU, HRVATSKA DEMOKRATSKA STRANKA, NACIONALNI DEMOKRATI - ND');
INSERT INTO election_list (id, name, description) VALUES (18, 'Lista Roberta Bosaka', 'AUSTRIJSKA BUGARSKA NJEMAČKA POLJSKA ROMSKA RUMUNJSKA RUSINSKA RUSKA TURSKA UKRAJINSKA VLAŠKA I ŽIDOVSKA NACIONALNA MANJINA, ROBERT BOSAK (kandidat), HRVATSKA DEMOKRATSKA ZAJEDNICA - HDZ');
INSERT INTO election_list (id, name, description) VALUES (19, 'HKS, HSP, OS', 'HRVATSKA KONZERVATIVNA STRANKA - HKS, HRVATSKA STRANKA PRAVA - HSP, OBITELJSKA STRANKA - OS ');
INSERT INTO election_list (id, name, description) VALUES (20, 'HSR', 'HRVATSKA STRANKA REDA - HSR');
INSERT INTO election_list (id, name, description) VALUES (21, 'HZ', 'HRVATSKA ZORA STRANKA NARODA - HZ');
INSERT INTO election_list (id, name, description) VALUES (22, 'HDSSB', 'HRVATSKI DEMOKRATSKI SAVEZ SLAVONIJE I BARANJE - HDSSB');
INSERT INTO election_list (id, name, description) VALUES (23, 'IDS, PGS, RI', 'ISTARSKI DEMOKRATSKI SABOR - IDS, PRIMORSKO GORANSKI SAVEZ - PGS, LISTA ZA RIJEKU - RI');
INSERT INTO election_list (id, name, description) VALUES (24, 'Lista Bari Ahmedija', 'BARI AHMEDI (kandidat), BARI AHMEDI - KANDIDAT GRUPE BIRAČA');
INSERT INTO election_list (id, name, description) VALUES (25, 'Lista Jonuza Alitija', 'ALBANSKA BOŠNJAČKA CRNOGORSKA MAKEDONSKA I SLOVENSKA NACIONALNA MANJINA, JONUZ ALITI (kandidat), JONUZ ALITI - KANDIDAT GRUPE BIRAČA');
INSERT INTO election_list (id, name, description) VALUES (26, 'Lista Vladimira Bileka', 'ČEŠKA I SLOVAČKA NACIONALNA MANJINA, VLADIMIR BILEK (kandidat), VLADIMIR BILEK - KANDIDAT GRUPE BIRAČA');
INSERT INTO election_list (id, name, description) VALUES (27, 'Lista Lutvije Grace', 'ALBANSKA BOŠNJAČKA CRNOGORSKA MAKEDONSKA I SLOVENSKA NACIONALNA MANJINA, LUTVIJA GRACA (kandidat), LUTVIJA GRACA - KANDIDAT GRUPE BIRAČA');
INSERT INTO election_list (id, name, description) VALUES (28, 'Lista Zvonka Kalanjoša', 'AUSTRIJSKA BUGARSKA NJEMAČKA POLJSKA ROMSKA RUMUNJSKA RUSINSKA RUSKA TURSKA UKRAJINSKA VLAŠKA I ŽIDOVSKA NACIONALNA MANJINA, ZVONKO KALANJOŠ (kandidat), ZVONKO KALANJOŠ - KANDIDAT GRUPE BIRAČA');
INSERT INTO election_list (id, name, description) VALUES (29, 'Lista Ivana Komaka', 'ČEŠKA I SLOVAČKA NACIONALNA MANJINA, IVAN KOMAK (kandidat), IVAN KOMAK - KANDIDAT GRUPE BIRAČA');
INSERT INTO election_list (id, name, description) VALUES (30, 'Lista Siniše Ljubojevića', 'SRPSKA NACIONALNA MANJINA, SINIŠA LJUBOJEVIĆ (kandidat), JOVICA RADMANOVIĆ (zamjenik), SRĐAN MILAKOVIĆ (kandidat), NEBOJŠA MANOJLOVIĆ (zamjenik), SINIŠA LJUBOJEVIĆ - KANDIDAT GRUPE BIRAČA');
INSERT INTO election_list (id, name, description) VALUES (31, 'Lista Furia Radina', 'TALIJANSKA NACIONALNA MANJINA, FURIO RADIN (kandidat), ROBERTO PALISCA (zamjenik), FURIO RADIN - KANDIDAT GRUPE BIRAČA');
INSERT INTO election_list (id, name, description) VALUES (32, 'Lista Suada Salkića', 'ALBANSKA BOŠNJAČKA CRNOGORSKA MAKEDONSKA I SLOVENSKA NACIONALNA MANJINA, SUAD SALKIĆ (kandidat), SUAD SALKIĆ - KANDIDAT GRUPE BIRAČA');
INSERT INTO election_list (id, name, description) VALUES (33, 'Lista Sulejmana Tabakovića', 'ALBANSKA BOŠNJAČKA CRNOGORSKA MAKEDONSKA I SLOVENSKA NACIONALNA MANJINA, SULEJMAN TABAKOVIĆ (kandidat), SULEJMAN TABAKOVIĆ - KANDIDAT GRUPE BIRAČA');
INSERT INTO election_list (id, name, description) VALUES (34, 'Lista Maurizia Zennara', 'TALIJANSKA NACIONALNA MANJINA, MAURIZIO ZENNARO (kandidat), ERNIE GIGANTE DEŠKOVIĆ (zamjenik), MAURIZIO ZENNARO - KANDIDAT GRUPE BIRAČA');
INSERT INTO election_list (id, name, description) VALUES (35, 'Lista Željka Baloga', 'AUSTRIJSKA BUGARSKA NJEMAČKA POLJSKA ROMSKA RUMUNJSKA RUSINSKA RUSKA TURSKA UKRAJINSKA VLAŠKA I ŽIDOVSKA NACIONALNA MANJINA, ŽELJKO BALOG (kandidat), NAPRIJED HRVATSKA! - PROGRESIVNI SAVEZ IVE JOSIPOVIĆA, KROVNA ZAJEDNICA ROMA HRVATSKE');
INSERT INTO election_list (id, name, description) VALUES (36, 'MDS', 'MEĐIMURSKI DEMOKRATSKI SAVEZ - MDS');
INSERT INTO election_list (id, name, description) VALUES (37, 'MOST', 'MOST NEZAVISNIH LISTA - MOST');
INSERT INTO election_list (id, name, description) VALUES (38, 'Lista Jovana Ajdukovića', 'SRPSKA NACIONALNA MANJINA, JOVAN AJDUKOVIĆ (kandidat), TATJANA LUKIĆ (zamjenica), SLAVKO MIRNIĆ (kandidat), ĐOKA RAŠIĆ (zamjenik), DRAGAN TODIĆ (kandidat), SAŠA MILETIĆ (zamjenik), NAŠA STRANKA - NS');
INSERT INTO election_list (id, name, description) VALUES (39, 'NEOVISNA LISTA - DAMIR BAJS', 'NEOVISNA LISTA - DAMIR BAJS');
INSERT INTO election_list (id, name, description) VALUES (40, 'NEOVISNA LISTA - MIRA DAVIDOVIĆ', 'NEOVISNA LISTA - MIRA DAVIDOVIĆ');
INSERT INTO election_list (id, name, description) VALUES (41, 'NEOVISNA LISTA - ŽELJKO KNAPIĆ', 'NEOVISNA LISTA - ŽELJKO KNAPIĆ');
INSERT INTO election_list (id, name, description) VALUES (42, 'NEOVISNA LISTA - ŽELJKO LACKOVIĆ ', 'NEOVISNA LISTA - ŽELJKO LACKOVIĆ ');
INSERT INTO election_list (id, name, description) VALUES (43, 'NEOVISNA LISTA - VESNA PINJUH', 'NEOVISNA LISTA - VESNA PINJUH');
INSERT INTO election_list (id, name, description) VALUES (44, 'Lista Renate Trischler', 'AUSTRIJSKA BUGARSKA NJEMAČKA POLJSKA ROMSKA RUMUNJSKA RUSINSKA RUSKA TURSKA UKRAJINSKA VLAŠKA I ŽIDOVSKA NACIONALNA MANJINA, RENATA TRISCHLER (kandidatkinja), NJEMAČKA ZAJEDNICA - ZEMALJSKA UDRUGA PODUNAVSKIH ŠVABA U HRVATSKOJ - DEUTSCHE GEMEINSCHAFT - LANDSMANNSCHAFT DER DONAUSCHWABEN IN KROATIEN');
INSERT INTO election_list (id, name, description) VALUES (45, 'ORaH', 'ODRŽIVI RAZVOJ HRVATSKE - ORaH');
INSERT INTO election_list (id, name, description) VALUES (46, 'Lista Daniele Dapas', 'TALIJANSKA NACIONALNA MANJINA, DANIELA DAPAS (kandidatkinja), LOVRE DE GRISOGONO (zamjenik), ODRŽIVI RAZVOJ HRVATSKE - ORaH');
INSERT INTO election_list (id, name, description) VALUES (47, 'PAMETNO', 'PAMETNO');
INSERT INTO election_list (id, name, description) VALUES (48, 'POKRET ZAJEDNO', 'POKRET ZAJEDNO');
INSERT INTO election_list (id, name, description) VALUES (49, 'RADNIČKA FRONTA - RF', 'RADNIČKA FRONTA - RF');
INSERT INTO election_list (id, name, description) VALUES (50, 'Lista Mile Horvata', 'SRPSKA NACIONALNA MANJINA, MILE HORVAT (kandidat), DRAGAN CRNOGORAC (zamjenik), MILORAD PUPOVAC (kandidat), BOGDAN RKMAN (zamjenik), MIRKO RAŠKOVIĆ (kandidat), VESNA BLANUŠA (zamjenica), SAMOSTALNA DEMOKRATSKA SRPSKA STRANKA - SDSS');
INSERT INTO election_list (id, name, description) VALUES (51, 'Lista Šandora Juhasa', 'MAĐARSKA NACIONALNA MANJINA, ŠANDOR JUHAS (kandidat), MARGITA BUDIM (zamjenica), SAVEZ MAĐARSKIH UDRUGA - SMU - MAGYAR EGYESÜLETEK SZÖVETSËGE - MESZ');
INSERT INTO election_list (id, name, description) VALUES (52, 'Lista Vesne Pichler', 'AUSTRIJSKA BUGARSKA NJEMAČKA POLJSKA ROMSKA RUMUNJSKA RUSINSKA RUSKA TURSKA UKRAJINSKA VLAŠKA I ŽIDOVSKA NACIONALNA MANJINA, VESNA PICHLER (kandidatkinja), SAVEZ NIJEMACA I AUSTRIJANACA HRVATSKE CENTRALA-OSIJEK - VEREIN DER DEUTCHEN UND OSTERREICHER KROATIEN ZENTRALE - OSIJEK');
INSERT INTO election_list (id, name, description) VALUES (53, 'Lista Jelene Zaričnaja Jindre', 'AUSTRIJSKA BUGARSKA NJEMAČKA POLJSKA ROMSKA RUMUNJSKA RUSINSKA RUSKA TURSKA UKRAJINSKA VLAŠKA I ŽIDOVSKA NACIONALNA MANJINA, JELENA ZARIČNAJA JINDRA (kandidatkinja), SAVEZ RUSA REPUBLIKE HRVATSKE - SAVEZ RUSA RH');
INSERT INTO election_list (id, name, description) VALUES (54, 'Lista Dubravke Rašljanin', 'AUSTRIJSKA BUGARSKA NJEMAČKA POLJSKA ROMSKA RUMUNJSKA RUSINSKA RUSKA TURSKA UKRAJINSKA VLAŠKA I ŽIDOVSKA NACIONALNA MANJINA, DUBRAVKA RAŠLJANIN (kandidatkinja), SAVEZ RUSINA REPUBLIKE HRVATSKE');
INSERT INTO election_list (id, name, description) VALUES (55, 'Lista Sabine Koželj-Horvat', 'ALBANSKA BOŠNJAČKA CRNOGORSKA MAKEDONSKA I SLOVENSKA NACIONALNA MANJINA, SABINA KOŽELJ-HORVAT (kandidatkinja), SLOVENSKO KULTURNO DRUŠTVO STANKO VRAZ');
INSERT INTO election_list (id, name, description) VALUES (56, 'SRP', 'SOCIJALISTIČKA RADNIČKA PARTIJA HRVATSKE - SRP');
INSERT INTO election_list (id, name, description) VALUES (57, 'Lista Nenada Vlahovića', 'SRPSKA NACIONALNA MANJINA, NENAD VLAHOVIĆ (kandidat), JOVO GRKINIĆ (zamjenik), DUŠAN BJELAJAC (kandidat), NIKOLA SUŽNJEVIĆ (zamjenik), DRAGOLJUB PETROVIĆ (kandidat), SANDA RAOS (zamjenica), SRPSKA PRAVEDNA STRANKA - SPS');
INSERT INTO election_list (id, name, description) VALUES (58, 'Lista Alije Avdića', 'ALBANSKA BOŠNJAČKA CRNOGORSKA MAKEDONSKA I SLOVENSKA NACIONALNA MANJINA, ALIJA AVDIĆ (kandidat), STRANKA BOŠNJAKA HRVATSKE - SBH');
INSERT INTO election_list (id, name, description) VALUES (59, 'Lista Mirsada Srebrnikovića', 'ALBANSKA BOŠNJAČKA CRNOGORSKA MAKEDONSKA I SLOVENSKA NACIONALNA MANJINA, MIRSAD SREBRENIKOVIĆ (kandidat), STRANKA DEMOKRATSKE AKCIJE HRVATSKE - SDA HRVATSKE');
INSERT INTO election_list (id, name, description) VALUES (60, 'U IME OBITELJI', 'U IME OBITELJI-PROJEKT DOMOVINA');
INSERT INTO election_list (id, name, description) VALUES (61, 'Lista Duška Kostića', 'AUSTRIJSKA BUGARSKA NJEMAČKA POLJSKA ROMSKA RUMUNJSKA RUSINSKA RUSKA TURSKA UKRAJINSKA VLAŠKA I ŽIDOVSKA NACIONALNA MANJINA, DUŠKO KOSTIĆ (kandidat), UDRUGA ROMSKOG PRIJATELJSTVA ''LUNA'' - LUNA');
INSERT INTO election_list (id, name, description) VALUES (62, 'Lista Veljka Kajtazija', 'AUSTRIJSKA BUGARSKA NJEMAČKA POLJSKA ROMSKA RUMUNJSKA RUSINSKA RUSKA TURSKA UKRAJINSKA VLAŠKA I ŽIDOVSKA NACIONALNA MANJINA, VELJKO KAJTAZI (kandidat), UDRUGA ZA PROMICANJE OBRAZOVANJA ROMA U REPUBLICI HRVATSKOJ ''KALI SARA'' - UZOR U RH ''KALI SARA''');
INSERT INTO election_list (id, name, description) VALUES (63, 'ZA GRAD', 'ZA GRAD');
INSERT INTO election_list (id, name, description) VALUES (64, 'Lista Idrisa Sulejmanija', 'ALBANSKA BOŠNJAČKA CRNOGORSKA MAKEDONSKA I SLOVENSKA NACIONALNA MANJINA, IDRIS SULEJMANI (kandidat), ZAJEDNICA ALBANACA ISTARSKE ŽUPANIJE');
INSERT INTO election_list (id, name, description) VALUES (65, 'ŽELJKO KERUM - HGS', 'ŽELJKO KERUM - HRVATSKA GRAĐANSKA STRANKA');


--
-- TOC entry 2076 (class 0 OID 22822)
-- Dependencies: 197
-- Data for Name: region; Type: TABLE DATA; Schema: public; Owner: sabor2015
--

INSERT INTO region (id, name) VALUES (1, 'I. izborna jedinica');
INSERT INTO region (id, name) VALUES (2, 'II. izborna jedinica');
INSERT INTO region (id, name) VALUES (3, 'III. izborna jedinica');
INSERT INTO region (id, name) VALUES (4, 'IV. izborna jedinica');
INSERT INTO region (id, name) VALUES (5, 'V. izborna jedinica');
INSERT INTO region (id, name) VALUES (6, 'VI. izborna jedinica');
INSERT INTO region (id, name) VALUES (7, 'VII. izborna jedinica');
INSERT INTO region (id, name) VALUES (8, 'VIII. izborna jedinica');
INSERT INTO region (id, name) VALUES (9, 'IX. izborna jedinica');
INSERT INTO region (id, name) VALUES (10, 'X. izborna jedinica');
INSERT INTO region (id, name) VALUES (11, 'XI. izborna jedinica (Dijaspora)');
INSERT INTO region (id, name) VALUES (12, 'XII. izborna jedinica (Manjine)');


--
-- TOC entry 2079 (class 0 OID 22840)
-- Dependencies: 200
-- Data for Name: election_list_regions; Type: TABLE DATA; Schema: public; Owner: sabor2015
--

INSERT INTO election_list_regions (election_list_id, region_id) VALUES (1, 1);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (1, 2);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (1, 3);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (1, 4);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (1, 5);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (1, 6);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (1, 7);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (1, 8);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (1, 10);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (2, 9);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (3, 1);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (3, 2);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (3, 3);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (3, 4);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (3, 5);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (3, 6);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (3, 7);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (3, 8);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (3, 10);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (4, 1);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (4, 2);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (4, 3);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (4, 4);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (4, 5);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (4, 6);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (4, 7);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (4, 8);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (4, 9);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (4, 10);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (5, 1);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (5, 2);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (5, 3);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (5, 4);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (5, 5);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (5, 6);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (5, 7);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (5, 8);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (5, 9);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (5, 10);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (5, 11);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (6, 1);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (6, 2);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (6, 3);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (6, 4);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (6, 5);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (6, 6);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (6, 7);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (6, 8);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (6, 9);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (6, 10);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (6, 11);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (7, 12);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (8, 1);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (8, 2);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (8, 3);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (8, 4);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (8, 5);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (8, 6);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (8, 7);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (8, 8);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (8, 9);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (8, 10);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (8, 11);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (9, 12);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (10, 12);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (11, 12);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (12, 1);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (12, 3);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (12, 4);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (12, 7);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (13, 12);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (14, 4);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (15, 1);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (15, 2);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (15, 3);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (15, 4);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (15, 5);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (15, 6);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (15, 7);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (15, 8);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (15, 9);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (15, 10);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (15, 11);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (16, 12);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (17, 1);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (17, 3);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (17, 5);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (17, 6);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (17, 7);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (17, 9);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (17, 10);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (17, 11);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (18, 12);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (19, 1);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (19, 2);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (19, 3);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (19, 4);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (19, 5);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (19, 6);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (19, 7);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (19, 8);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (19, 9);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (19, 10);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (19, 11);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (20, 2);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (21, 1);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (21, 2);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (21, 6);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (21, 7);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (21, 9);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (21, 10);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (22, 4);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (22, 5);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (23, 8);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (24, 12);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (25, 12);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (26, 12);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (27, 12);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (28, 12);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (29, 12);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (30, 12);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (31, 12);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (32, 12);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (33, 12);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (34, 12);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (35, 12);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (36, 3);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (37, 1);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (37, 2);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (37, 3);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (37, 4);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (37, 5);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (37, 6);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (37, 7);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (37, 8);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (37, 9);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (37, 10);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (37, 11);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (38, 12);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (39, 2);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (40, 1);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (41, 3);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (42, 2);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (43, 11);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (44, 12);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (45, 1);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (45, 2);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (45, 3);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (45, 4);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (45, 5);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (45, 6);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (45, 7);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (45, 8);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (45, 9);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (45, 10);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (45, 11);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (46, 12);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (47, 1);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (47, 7);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (47, 10);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (48, 1);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (48, 7);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (48, 8);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (49, 1);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (49, 6);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (49, 8);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (50, 12);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (51, 12);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (52, 12);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (53, 12);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (54, 12);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (55, 12);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (56, 2);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (56, 3);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (56, 8);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (56, 10);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (56, 11);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (57, 12);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (58, 12);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (59, 12);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (60, 1);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (60, 2);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (60, 3);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (60, 4);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (60, 5);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (60, 6);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (60, 7);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (60, 8);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (60, 9);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (60, 10);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (60, 11);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (61, 12);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (62, 12);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (63, 1);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (63, 2);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (63, 6);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (63, 7);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (64, 12);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (65, 9);
INSERT INTO election_list_regions (election_list_id, region_id) VALUES (65, 10);


--
-- TOC entry 2077 (class 0 OID 22827)
-- Dependencies: 198
-- Data for Name: party; Type: TABLE DATA; Schema: public; Owner: sabor2015
--

INSERT INTO party (id, name) VALUES (1, 'AKCIJA MLADIH - AM');
INSERT INTO party (id, name) VALUES (2, 'AUTOHTONA - HRVATSKA SELJAČKA STRANKA - A - HS');
INSERT INTO party (id, name) VALUES (3, 'AUTOHTONA - HRVATSKA STRANKA PRAVA - A - HSP');
INSERT INTO party (id, name) VALUES (4, 'BANDIĆ MILAN 365 - STRANKA RADA I SOLIDARNOSTI');
INSERT INTO party (id, name) VALUES (5, 'BLOK UMIROVLJENICI ZAJEDNO - BUZ');
INSERT INTO party (id, name) VALUES (6, 'BRANITELJSKO DOMOLJUBNA STRANKA HRVATSKE');
INSERT INTO party (id, name) VALUES (7, 'DEMOKRATSKA PRIGORSKO - ZAGREBAČKA STRANKA - DPS');
INSERT INTO party (id, name) VALUES (8, 'DEMOKRATSKA STRANKA ŽENA - DSŽ');
INSERT INTO party (id, name) VALUES (9, 'DEMOKRATSKI SAVEZ NACIONALNE OBNOVE - DESNO');
INSERT INTO party (id, name) VALUES (10, 'DUBROVAČKI DEMOKRATSKI SABOR - DDS');
INSERT INTO party (id, name) VALUES (11, 'HRAST - POKRET ZA USPJEŠNU HRVATSKU');
INSERT INTO party (id, name) VALUES (12, 'HRVATSKA DEMOKRATSKA STRANKA');
INSERT INTO party (id, name) VALUES (13, 'HRVATSKA DEMOKRATSKA ZAJEDNICA - HDZ');
INSERT INTO party (id, name) VALUES (14, 'HRVATSKA DEMOKRŠĆANSKA STRANKA - HDS');
INSERT INTO party (id, name) VALUES (15, 'HRVATSKA EUROPSKA STRANKA - HES');
INSERT INTO party (id, name) VALUES (16, 'HRVATSKA KONZERVATIVNA STRANKA - HKS');
INSERT INTO party (id, name) VALUES (17, 'HRVATSKA KRŠĆANSKA DEMOKRATSKA UNIJA - HKDU');
INSERT INTO party (id, name) VALUES (18, 'HRVATSKA NARODNA STRANKA - LIBERALNI DEMOKRATI - HNS');
INSERT INTO party (id, name) VALUES (19, 'HRVATSKA RADNIČKA STRANKA - HRS');
INSERT INTO party (id, name) VALUES (20, 'HRVATSKA SELJAČKA STRANKA - HSS');
INSERT INTO party (id, name) VALUES (21, 'HRVATSKA SOCIJALNO - LIBERALNA STRANKA - HSLS');
INSERT INTO party (id, name) VALUES (22, 'HRVATSKA STRANKA PRAVA - HSP');
INSERT INTO party (id, name) VALUES (23, 'HRVATSKA STRANKA PRAVA DR. ANTE STARČEVIĆ - HSP AS');
INSERT INTO party (id, name) VALUES (24, 'HRVATSKA STRANKA REDA - HSR');
INSERT INTO party (id, name) VALUES (25, 'HRVATSKA STRANKA UMIROVLJENIKA - HSU');
INSERT INTO party (id, name) VALUES (26, 'HRVATSKA STRANKA ZELENIH - EKO SAVEZ - ZELENI');
INSERT INTO party (id, name) VALUES (27, 'HRVATSKA ZORA STRANKA NARODA - HZ');
INSERT INTO party (id, name) VALUES (28, 'HRVATSKI DEMOKRATSKI SAVEZ SLAVONIJE I BARANJE - HDSSB');
INSERT INTO party (id, name) VALUES (29, 'HRVATSKI LABURISTI - STRANKA RADA');
INSERT INTO party (id, name) VALUES (30, 'ISTARSKI DEMOKRATI - DEMOCRATICI ISTRIANI - ID - DI');
INSERT INTO party (id, name) VALUES (31, 'ISTARSKI DEMOKRATSKI SABOR - IDS');
INSERT INTO party (id, name) VALUES (32, 'LISTA ZA RIJEKU - RI');
INSERT INTO party (id, name) VALUES (33, 'MEĐIMURSKA STRANKA - MS');
INSERT INTO party (id, name) VALUES (34, 'MEĐIMURSKI DEMOKRATSKI SAVEZ - MDS');
INSERT INTO party (id, name) VALUES (35, 'MOST NEZAVISNIH LISTA - MOST');
INSERT INTO party (id, name) VALUES (36, 'NACIONALNI DEMOKRATI - ND');
INSERT INTO party (id, name) VALUES (37, 'NAPRIJED HRVATSKA! - PROGRESIVNI SAVEZ IVE JOSIPOVIĆA');
INSERT INTO party (id, name) VALUES (38, 'NARODNA STRANKA - REFORMISTI - REFORMISTI');
INSERT INTO party (id, name) VALUES (39, 'NEOVISNA LISTA - DAMIR BAJS');
INSERT INTO party (id, name) VALUES (40, 'NEOVISNA LISTA - MIRA DAVIDOVIĆ');
INSERT INTO party (id, name) VALUES (41, 'NEOVISNA LISTA - VESNA PINJUH');
INSERT INTO party (id, name) VALUES (42, 'NEOVISNA LISTA - ŽELJKO KNAPIĆ');
INSERT INTO party (id, name) VALUES (43, 'NEOVISNA LISTA - ŽELJKO LACKOVIĆ');
INSERT INTO party (id, name) VALUES (44, 'NEZAVISNI SELJACI HRVATSKE - NSH');
INSERT INTO party (id, name) VALUES (45, 'NOVI VAL - STRANKA RAZVOJA - NOVI VAL');
INSERT INTO party (id, name) VALUES (46, 'OBITELJSKA STRANKA - OS');
INSERT INTO party (id, name) VALUES (47, 'ODRŽIVI RAZVOJ HRVATSKE - ORaH');
INSERT INTO party (id, name) VALUES (48, 'PAMETNO');
INSERT INTO party (id, name) VALUES (49, 'POKRET ZAJEDNO');
INSERT INTO party (id, name) VALUES (50, 'PRIMORSKO GORANSKI SAVEZ - PGS');
INSERT INTO party (id, name) VALUES (51, 'RADNIČKA FRONTA - RF');
INSERT INTO party (id, name) VALUES (52, 'SAMOSTALNA DEMOKRATSKA SRPSKA STRANKA - SDSS');
INSERT INTO party (id, name) VALUES (53, 'SOCIJALDEMOKRATSKA PARTIJA HRVATSKE - SDP');
INSERT INTO party (id, name) VALUES (54, 'SOCIJALISTIČKA RADNIČKA PARTIJA HRVATSKE - SRP');
INSERT INTO party (id, name) VALUES (55, 'STRANKA HRVATSKIH UMIROVLJENIKA - UMIROVLJENICI');
INSERT INTO party (id, name) VALUES (56, 'STRANKA UMIROVLJENIKA - SU');
INSERT INTO party (id, name) VALUES (57, 'U IME OBITELJI-PROJEKT DOMOVINA');
INSERT INTO party (id, name) VALUES (58, 'UMIROVLJENIČKA DEMOKRATSKA UNIJA - UDU');
INSERT INTO party (id, name) VALUES (59, 'ZA GRAD');
INSERT INTO party (id, name) VALUES (60, 'ZAGORSKA DEMOKRATSKA STRANKA - ZDS');
INSERT INTO party (id, name) VALUES (61, 'ZAGORSKA STRANKA - ZS');
INSERT INTO party (id, name) VALUES (62, 'ZELENA STRANKA - ZS');
INSERT INTO party (id, name) VALUES (63, 'ZELENI FORUM');
INSERT INTO party (id, name) VALUES (64, 'ZELENI SAVEZ - ZELENI');
INSERT INTO party (id, name) VALUES (65, 'ŽELJKO KERUM - HRVATSKA GRAĐANSKA STRANKA');
INSERT INTO party (id, name) VALUES (66, 'ŽIVI ZID');

